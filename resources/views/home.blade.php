@extends('frontend')

@section('title')
    Trang chủ - ABC Kitchen
@stop

@section('content')
    @include('frontend/slideshow')
    <hr></hr>
    @include('frontend/todaymenu')
@stop
