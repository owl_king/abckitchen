@extends('...backend')

@section('title')
    Quản trị viên
@stop

@section('content')
    	<div class="row">
            <div class="col-md-12">
            <div class="row">
            <div class="col-md-4">
                <button class="btn btn-success btn-xs btn-all" data-toggle="modal" data-target="#delete" >Hủy đánh dấu&nbsp;<span class="glyphicon glyphicon-ban-circle"></span></button>
                <button class="btn btn-danger btn-xs btn-all" data-toggle="modal" data-target="#delete" >Xóa nhiều&nbsp;<span class="glyphicon glyphicon-trash"></span></button>
            </div>
            <div class="col-md-4 title-search">
            <form action="" class="search-form">
                <div class="form-group has-feedback">
                    <label for="search" class="sr-only">Search</label>
                    <input type="text" class="form-control" name="search" id="search" placeholder="Tìm kiếm">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </form>
            </div>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-users"></i> DANH SÁCH QUẢN TRỊ VIÊN
                    </li>
                </ol>
            {{--<div class="col-md-4 title-search"><h2 style="color:#77BC1B; font-weight: bold"><i class="fa fa-users"></i>DANH SÁCH QUẢN TRỊ VIÊN</h2></div>--}}
            </div>
            <hr style="height: 0px">
            <div class="table-responsive">
                  <table id="mytable" class="table table-bordred table-striped">

                      <thead>

                      <th><input type="checkbox" id="checkall" /></th>
                      <th><a class="title-row" href="{{ "showManagers?page=1&title=name&sort=".((isset($data) && !empty($data))?(($data['sort']=='asc')?("desc"):("asc")):"asc") }}">
                              Tên tài khoản
                          </a><span class="{{ (isset($data) && !empty($data))?(($data['title']=='name')?(($data['sort']=='asc')?'glyphicon glyphicon-chevron-down':'glyphicon glyphicon-chevron-up'):""):"" }}"></span>
                      </th>
                      <th>Email</th>
                      <th><a class="title-row" href="{{ "showManagers?page=1&title=created_at&sort=".((isset($data) && !empty($data))?(($data['sort']=='asc')?("desc"):("asc")):"asc") }}">
                              Ngày tạo
                          </a><span class="{{ (isset($data) && !empty($data))?(($data['title']=='created_at')?(($data['sort']=='asc')?'glyphicon glyphicon-chevron-down':'glyphicon glyphicon-chevron-up'):""):"" }}"></span>
                      </th>
                      <th><a class="title-row" href="{{ "showManagers?page=1&title=updated_at&sort=".((isset($data) && !empty($data))?(($data['sort']=='asc')?("desc"):("asc")):"asc") }}">
                              Ngày sửa
                          </a><span class="{{ (isset($data) && !empty($data))?(($data['title']=='updated_at')?(($data['sort']=='asc')?'glyphicon glyphicon-chevron-down':'glyphicon glyphicon-chevron-up'):""):"" }}"></span>
                      </th>
                      <th>Liên hệ</th>
                      <th>Sửa</th>
                      <th>Xóa</th>
                      </thead>
        <tbody>

        @foreach($allAdm as $adm)
            <tr>
                <td><input type="checkbox" class="checkthis" /></td>
                <td>{{ $adm->name }}</td>
                <td>{{ $adm->email }}</td>
                <td>{{ $adm->created_at->format('d-m-Y') }}</td>
                <td>{{ $adm->updated_at->format('d-m-Y') }}</td>
                <td>-</td>
                <td><p data-placement="top"  title="Edit"><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#edit-manager" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                <td><p data-placement="top"  title="Delete"><button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-manager" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
        @endforeach

        </tbody>

    </table>

    <div class="clearfix"></div>
                {!! (isset($data) && !empty($data))?($allAdm->appends(['title' => $data['title'], 'sort' => $data['sort']])->render()):($allAdm->render()) !!}

                </div>

            </div>
    	</div>
    </div>


    <div class="modal fade" id="edit-manager" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
        <div class="modal-content">
              <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            <h4 class="modal-title custom_align" id="Heading">Chỉnh sửa thông tin</h4>
          </div>
              <div class="modal-body">
              <div class="form-group">
            <input class="form-control " type="text" placeholder="Mohsin">
            </div>
            <div class="form-group">

            <input class="form-control " type="text" placeholder="Irshad">
            </div>
            <div class="form-group">
            <textarea rows="2" class="form-control" placeholder="CB 106/107 Street # 11 Wah Cantt Islamabad Pakistan"></textarea>


            </div>
          </div>
              <div class="modal-footer ">
            <button type="button" class="btn btn-primary btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Cập nhật</button>
          </div>
            </div>
        <!-- /.modal-content -->
      </div>
          <!-- /.modal-dialog -->
        </div>


        <div class="modal fade" id="delete-manager" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
        <div class="modal-content">
              <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            <h4 class="modal-title custom_align" id="Heading">Xóa tài khoản</h4>
          </div>
              <div class="modal-body">

           <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Bạn có chắc muốn xóa tài khoản này?</div>

          </div>
            <div class="modal-footer ">
            <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Có</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Không</button>
          </div>
            </div>
        <!-- /.modal-content -->
      </div>
          <!-- /.modal-dialog -->
</div>
@stop