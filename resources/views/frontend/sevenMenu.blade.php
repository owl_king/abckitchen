<label class="label-seven-menu">
    {{--<h4>Ngày/Tháng</h4>--}}
    <div class="day-show-seven">00/00</div>
    <h4>Thành tiền</h4>
    <h4 id="cost-seven-menu">0</h4>
</label>

<label class="label-seven-menu">
    <h4>Trạng thái</h4>
</label>
<div class="alert alert-warning alert-seven" role="alert"><h4>Bạn chưa đăng ký</h4></div>


<div id="mi-slider" class="mi-slider">

    <nav>
        <a href="#"></a>
        <a href="#"></a>
        <a href="#"></a>
        <a href="#"></a>
        <a href="#"></a>
        <a href="#"></a>
        <a href="#"></a>
    </nav>
    <div class="ch-grid-btn seven-ch-grid-btn">
        <div class="seven-li-btn li-btn li-seven-menu">
            <a id="submit-seven-menu" class="res-seven-menu">
                <div class="ch-item">
                    <div class="ch-info">
                        <div class="ch-info-front ch-img-1">
                            <h3>Đăng ký ngay</h3>
                        </div>
                        <div class="ch-info-back">
                            <i class="fa fa-plus-square"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
