<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Auth;

Route::group(['middleware' => 'checkmem'], function(){
    //trang tuong tac nguoi dung
    Route::group(['before' => 'csrf'], function() {
        Route::get('/', 'FrontendController@home');
        Route::get('home', 'FrontendController@home');
        Route::get('contact', 'FrontendController@contact');
        Route::get('costtotal', 'FrontendController@costtotal');
        Route::post('costtotal/showdetails/{id}', 'FrontendController@showDetails');
        Route::post('costtotal/showbymonth/{id}', 'FrontendController@showByMonth');
        Route::get('ordermenu', 'FrontendController@orderMenu');
        Route::post('ordermenu/tomorrowmenu', 'FrontendController@tomorrowMenu');
        Route::post('ordermenu/menuregister/{id}', 'FrontendController@MenuRegister');
        Route::post('ordermenu/menudelete/{id}', 'FrontendController@MenuDelete');
        Route::post('ordermenu/sevenmenu', 'FrontendController@sevenMenu');
        Route::post('ordermenu/fixedmenu', 'FrontendController@fixedMenu');
        Route::post('ordermenu/fixedmenures', 'FrontendController@fixedMenuRes');
    });
});

Route::group(['middleware' => 'checkadmin'], function(){
    //trang quan tri admin
    Route::group(['before' => 'csrf','prefix' => 'admin'], function(){
        Route::get('/', 'BackendController@home');
        Route::get('menus/showCalendar', 'BackendController@showCalendar');
        Route::get('menus', 'BackendController@menus');
        Route::get('foods', 'BackendController@foods');
        Route::post('foods/createfood', 'BackendController@createFood');
        Route::get('foods/showfood', 'BackendController@showFood');
        Route::post('foods/editFood/{id}', 'BackendController@editFood');
        Route::post('foods/deleteFood/{id}', 'BackendController@deleteFood');
        Route::get('users/showManagers', 'BackendController@showManagers');
        Route::get('users/showMembers', 'BackendController@showMembers');
        Route::post('menus/showMenu/{id}', 'BackendController@showMenu');
        Route::post('menus/createmenu', 'BackendController@createMenu');
        Route::post('menus/deletemenu/{id}', 'BackendController@deleteMenu');
        Route::post('menus/editmenu', 'BackendController@editMenu');
        Route::post('users/showMembers/showpay/{id}', 'BackendController@showpay');
        Route::post('users/showMembers/showpay/{id}/showmonthadmin/{month}', 'BackendController@showMonthAdmin');
        Route::post('users/showMembers/showpay/{id}/submitstate/{month}', 'BackendController@submitState');
        Route::get('users/showMembers/search', 'BackendController@searchMem');
    });

});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
