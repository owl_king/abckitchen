@extends('...backend')

@section('title')
    Trang quản trị nhà ăn
@stop

@section('content')
    <div style="min-height: 342px;" id="page-wrapper">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-home"></i> TRANG CHỦ THỐNG KÊ
                </li>
            </ol>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-male fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge" id="khauphanngaymai">{{ $count_khauphanngaymai }}</div>
                                <div>Khẩu phần ngày mai</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Xem chi tiết</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-female fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge" id="khauphanhomnay">{{ $count_khauphanhomnay }}</div>
                                <div>Khẩu phần hôm nay</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Xem chi tiết</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-child fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge" id="khauphandaphat">0</div>
                                <div>Khẩu phần đã phát</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Xem chi tiết</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-support fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">2</div>
                                <div>Ý kiến phản hồi</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">Xem chi tiết</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Bảng thống kê nhà ăn ABC
                        <div class="pull-right">
                            <div class="btn-group">
                                {{--<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">--}}
                                    {{--Thao tác--}}
                                    {{--<span class="caret"></span>--}}
                                {{--</button>--}}
                                {{--<ul class="dropdown-menu pull-right" role="menu">--}}
                                    {{--<li><a href="#"></a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Another action</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Something else here</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="divider"></li>--}}
                                    {{--<li><a href="#">Separated link</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div style="position: relative;" id="morris-area-chart"><svg style="overflow: hidden; position: relative; top: -0.75px;" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" width="650" version="1.1" height="367"><desc>Created with Raphaël 2.1.2</desc><defs></defs><text font-weight="normal" fill="#888888" stroke="none" font-size="12px" font-family="sans-serif" text-anchor="end" y="333" x="53.5" style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"><tspan dy="4">0</tspan></text><path stroke-width="0.5" d="M66,333H625" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" fill="#888888" stroke="none" font-size="12px" font-family="sans-serif" text-anchor="end" y="256" x="53.5" style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"><tspan dy="4">7,500</tspan></text><path stroke-width="0.5" d="M66,256H625" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" fill="#888888" stroke="none" font-size="12px" font-family="sans-serif" text-anchor="end" y="179" x="53.5" style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"><tspan dy="4">15,000</tspan></text><path stroke-width="0.5" d="M66,179H625" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" fill="#888888" stroke="none" font-size="12px" font-family="sans-serif" text-anchor="end" y="102" x="53.5" style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"><tspan dy="4">22,500</tspan></text><path stroke-width="0.5" d="M66,102H625" stroke="#aaaaaa" fill="none" style=""></path><text font-weight="normal" fill="#888888" stroke="none" font-size="12px" font-family="sans-serif" text-anchor="end" y="25" x="53.5" style="text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"><tspan dy="4">30,000</tspan></text><path stroke-width="0.5" d="M66,25H625" stroke="#aaaaaa" fill="none" style=""></path><text transform="matrix(1,0,0,1,0,7)" font-weight="normal" fill="#888888" stroke="none" font-size="12px" font-family="sans-serif" text-anchor="middle" y="345.5" x="521.7582017010936" style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"><tspan dy="4">Tháng 6</tspan></text><text transform="matrix(1,0,0,1,0,7)" font-weight="normal" fill="#888888" stroke="none" font-size="12px" font-family="sans-serif" text-anchor="middle" y="345.5" x="273.84204131227216" style="text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"><tspan dy="4">Tháng 5</tspan></text><path fill-opacity="1" d="M66,278.4532C81.6221142162819,272.80653333333333,112.8663426488457,261.08841666666666,128.4884568651276,255.86653333333334C144.1105710814095,250.64465,175.3547995139733,243.82665063752276,190.97691373025518,236.67813333333334C206.42922235722966,229.60731730418942,237.33383961117863,201.09904254143646,252.78614823815312,198.98919999999998C268.0686512758202,196.90254254143645,298.6336573511543,218.35763333333333,313.91616038882137,219.89213333333333C329.53827460510325,221.46073333333334,360.7825030376671,210.35696666666666,376.40461725394897,211.4016C392.02673147023086,212.44623333333334,423.2709599027947,246.7927635701275,438.8930741190766,228.2492C454.3453827460511,209.90719690346083,485.25,72.80673333333333,500.7023086269745,63.859333333333325C516.154617253949,54.91193333333332,547.0592345078979,143.55070072859743,562.5115431348725,156.67C578.1336573511544,169.9334673952641,609.377885783718,166.2103,625,169.3904L625,333L66,333Z" stroke="none" fill="#7cb47c" style="fill-opacity: 1;"></path><path stroke-width="3" d="M66,278.4532C81.6221142162819,272.80653333333333,112.8663426488457,261.08841666666666,128.4884568651276,255.86653333333334C144.1105710814095,250.64465,175.3547995139733,243.82665063752276,190.97691373025518,236.67813333333334C206.42922235722966,229.60731730418942,237.33383961117863,201.09904254143646,252.78614823815312,198.98919999999998C268.0686512758202,196.90254254143645,298.6336573511543,218.35763333333333,313.91616038882137,219.89213333333333C329.53827460510325,221.46073333333334,360.7825030376671,210.35696666666666,376.40461725394897,211.4016C392.02673147023086,212.44623333333334,423.2709599027947,246.7927635701275,438.8930741190766,228.2492C454.3453827460511,209.90719690346083,485.25,72.80673333333333,500.7023086269745,63.859333333333325C516.154617253949,54.91193333333332,547.0592345078979,143.55070072859743,562.5115431348725,156.67C578.1336573511544,169.9334673952641,609.377885783718,166.2103,625,169.3904" stroke="#4da74d" fill="none" style=""></path><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="278.4532" cx="66"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="255.86653333333334" cx="128.4884568651276"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="236.67813333333334" cx="190.97691373025518"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="198.98919999999998" cx="252.78614823815312"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="219.89213333333333" cx="313.91616038882137"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="211.4016" cx="376.40461725394897"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="228.2492" cx="438.8930741190766"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="63.859333333333325" cx="500.7023086269745"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="156.67" cx="562.5115431348725"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#4da74d" r="2" cy="169.3904" cx="625"></circle><path fill-opacity="1" d="M66,305.6290666666667C81.6221142162819,299.45366666666666,112.8663426488457,286.33671666666663,128.4884568651276,280.92746666666665C144.1105710814095,275.51821666666666,175.3547995139733,265.31253989071035,190.97691373025518,262.35506666666663C206.42922235722966,259.4297398907103,237.33383961117863,259.77451731123386,252.78614823815312,257.39626666666663C268.0686512758202,255.0441506445672,298.6336573511543,246.73233076923077,313.91616038882137,243.4336C329.53827460510325,240.06156410256412,360.7825030376671,230.57331666666667,376.40461725394897,230.7132C392.02673147023086,230.85308333333333,423.2709599027947,258.84583023679414,438.8930741190766,244.55266666666665C454.3453827460511,230.4148635701275,485.25,125.37206666666665,500.7023086269745,116.98933333333332C516.154617253949,108.60659999999999,547.0592345078979,168.69056958105648,562.5115431348725,177.4908C578.1336573511544,186.38773624772313,609.377885783718,185.2062,625,187.778L625,333L66,333Z" stroke="none" fill="#a7b3bc" style="fill-opacity: 1;"></path><path stroke-width="3" d="M66,305.6290666666667C81.6221142162819,299.45366666666666,112.8663426488457,286.33671666666663,128.4884568651276,280.92746666666665C144.1105710814095,275.51821666666666,175.3547995139733,265.31253989071035,190.97691373025518,262.35506666666663C206.42922235722966,259.4297398907103,237.33383961117863,259.77451731123386,252.78614823815312,257.39626666666663C268.0686512758202,255.0441506445672,298.6336573511543,246.73233076923077,313.91616038882137,243.4336C329.53827460510325,240.06156410256412,360.7825030376671,230.57331666666667,376.40461725394897,230.7132C392.02673147023086,230.85308333333333,423.2709599027947,258.84583023679414,438.8930741190766,244.55266666666665C454.3453827460511,230.4148635701275,485.25,125.37206666666665,500.7023086269745,116.98933333333332C516.154617253949,108.60659999999999,547.0592345078979,168.69056958105648,562.5115431348725,177.4908C578.1336573511544,186.38773624772313,609.377885783718,185.2062,625,187.778" stroke="#7a92a3" fill="none" style=""></path><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="305.6290666666667" cx="66"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="280.92746666666665" cx="128.4884568651276"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="262.35506666666663" cx="190.97691373025518"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="257.39626666666663" cx="252.78614823815312"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="243.4336" cx="313.91616038882137"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="230.7132" cx="376.40461725394897"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="244.55266666666665" cx="438.8930741190766"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="116.98933333333332" cx="500.7023086269745"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="177.4908" cx="562.5115431348725"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#7a92a3" r="2" cy="187.778" cx="625"></circle><path fill-opacity="1" d="M66,305.6290666666667C81.6221142162819,305.3416,112.8663426488457,307.36156666666665,128.4884568651276,304.4792C144.1105710814095,301.59683333333334,175.3547995139733,283.84628561020037,190.97691373025518,282.57013333333333C206.42922235722966,281.30785227686704,237.33383961117863,296.7746906077348,252.78614823815312,294.32546666666667C268.0686512758202,291.90315727440145,298.6336573511543,265.49934615384615,313.91616038882137,263.084C329.53827460510325,260.6149794871795,360.7825030376671,272.2341666666667,376.40461725394897,274.788C392.02673147023086,277.34183333333334,423.2709599027947,295.6477908925319,438.8930741190766,283.51466666666664C454.3453827460511,271.5134242258652,485.25,185.77984999999998,500.7023086269745,178.25053333333332C516.154617253949,170.72121666666666,547.0592345078979,214.80408834244076,562.5115431348725,223.2801333333333C578.1336573511544,231.84932167577412,609.377885783718,240.64363333333333,625,246.43146666666667L625,333L66,333Z" stroke="none" fill="#2577b5" style="fill-opacity: 1;"></path><path stroke-width="3" d="M66,305.6290666666667C81.6221142162819,305.3416,112.8663426488457,307.36156666666665,128.4884568651276,304.4792C144.1105710814095,301.59683333333334,175.3547995139733,283.84628561020037,190.97691373025518,282.57013333333333C206.42922235722966,281.30785227686704,237.33383961117863,296.7746906077348,252.78614823815312,294.32546666666667C268.0686512758202,291.90315727440145,298.6336573511543,265.49934615384615,313.91616038882137,263.084C329.53827460510325,260.6149794871795,360.7825030376671,272.2341666666667,376.40461725394897,274.788C392.02673147023086,277.34183333333334,423.2709599027947,295.6477908925319,438.8930741190766,283.51466666666664C454.3453827460511,271.5134242258652,485.25,185.77984999999998,500.7023086269745,178.25053333333332C516.154617253949,170.72121666666666,547.0592345078979,214.80408834244076,562.5115431348725,223.2801333333333C578.1336573511544,231.84932167577412,609.377885783718,240.64363333333333,625,246.43146666666667" stroke="#0b62a4" fill="none" style=""></path><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="305.6290666666667" cx="66"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="304.4792" cx="128.4884568651276"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="282.57013333333333" cx="190.97691373025518"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="294.32546666666667" cx="252.78614823815312"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="263.084" cx="313.91616038882137"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="274.788" cx="376.40461725394897"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="283.51466666666664" cx="438.8930741190766"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="178.25053333333332" cx="500.7023086269745"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="223.2801333333333" cx="562.5115431348725"></circle><circle stroke-width="1" style="" stroke="#ffffff" fill="#0b62a4" r="2" cy="246.43146666666667" cx="625"></circle></svg><div style="left: 9px; top: 192px; display: none;" class="morris-hover morris-default-style"><div class="morris-hover-row-label">2010 Q1</div><div class="morris-hover-point" style="color: #0b62a4">
                                    iPhone:
                                    2,666
                                </div><div class="morris-hover-point" style="color: #7A92A3">
                                    iPad:
                                    -
                                </div><div class="morris-hover-point" style="color: #4da74d">
                                    iPod Touch:
                                    2,647
                                </div></div></div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!-- /.col-lg-8 -->
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bell fa-fw"></i> Thông báo chung
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="list-group">
                            <a href="#" class="list-group-item">
                                <i class="fa fa-comment fa-fw"></i> Bình luận mới
                                    <span class="pull-right text-muted small"><em>4 minutes ago</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-twitter fa-fw"></i> 3 theo dõi mới
                                    <span class="pull-right text-muted small"><em>12 minutes ago</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small"><em>27 minutes ago</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small"><em>43 minutes ago</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small"><em>11:32 AM</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-bolt fa-fw"></i> Server Crashed!
                                    <span class="pull-right text-muted small"><em>11:13 AM</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-warning fa-fw"></i> Server Not Responding
                                    <span class="pull-right text-muted small"><em>10:57 AM</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-shopping-cart fa-fw"></i> New Order Placed
                                    <span class="pull-right text-muted small"><em>9:49 AM</em>
                                    </span>
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="fa fa-money fa-fw"></i> Payment Received
                                    <span class="pull-right text-muted small"><em>Yesterday</em>
                                    </span>
                            </a>
                        </div>
                        <!-- /.list-group -->
                        <a href="#" class="btn btn-default btn-block">View All Alerts</a>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>
@stop