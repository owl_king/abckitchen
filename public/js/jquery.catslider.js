;( function( $, window, undefined ) {

	'use strict';

	$.CatSlider = function( options, element ) {
		this.$el = $( element );
		this._init( options );
	};

	$.CatSlider.prototype = {

		_init : function( options ) {

			// the categories (ul)
			this.$categories = this.$el.children( 'ul' );
			// the navigation
			this.$navcategories = this.$el.find( 'nav > a' );
			var animEndEventNames = {
				'WebkitAnimation' : 'webkitAnimationEnd',
				'OAnimation' : 'oAnimationEnd',
				'msAnimation' : 'MSAnimationEnd',
				'animation' : 'animationend'
			};
			// animation end event name
			this.animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ];
			// animations and transforms support
			this.support = Modernizr.csstransforms && Modernizr.cssanimations;
			// if currently animating
			this.isAnimating = false;
			// current category
			this.current = 0;
			var $currcat = this.$categories.eq( 0 );
			if( !this.support ) {
				this.$categories.hide();
				$currcat.show();
			}
			else {
				$currcat.addClass( 'mi-current' );
                $('.mi-slider ul').each(function (index) {
                    //console.log($(this).hasClass('mi-current'));
                    if ($(this).hasClass('mi-current') == false){
                        $(this).css('opacity', '0');
                    }
                });
			}
			// current nav category
			this.$navcategories.eq( 0 ).addClass( 'mi-selected' );
			// initialize the events
			this._initEvents();

		},
		_initEvents : function() {

			var self = this;
			this.$navcategories.on( 'click.catslider', function() {
				self.showCategory( $( this ).index() );
				return false;
			} );

			// reset on window resize..
			$( window ).on( 'resize', function() {
				self.$categories.removeClass().eq( 0 ).addClass( 'mi-current' );

				self.$navcategories.eq( self.current ).removeClass( 'mi-selected' ).end().eq( 0 ).addClass( 'mi-selected' );
				self.current = 0;
			} );

		},
		showCategory : function( catidx ) {

			if( catidx === this.current || this.isAnimating ) {
				return false;
			}
			this.isAnimating = true;
			// update selected navigation
            //khi an vao ngay //
			this.$navcategories.eq( this.current ).removeClass( 'mi-selected' ).end().eq( catidx ).addClass( 'mi-selected' );
            //hien ngay dang click
            $('.day-show-seven').html('<i class="fa fa-calendar"></i> '+this.$navcategories.eq( this.current ).end().eq(catidx).html());
            //hien gia thanh tuong ung voi tung ngay
            $('.label-seven-menu #cost-seven-menu').html(this.$navcategories.eq( this.current ).end().eq(catidx).attr('total_cost'));
            //hien xem nguoi dung da dang ky chua
            if (this.$navcategories.eq( this.current ).end().eq(catidx).attr('check_reg')=='true'){
                $('.alert-seven').removeClass('alert-warning');
                $('.alert-seven').addClass('alert-success');
                $('.alert-seven h4').html('Bạn đã đăng ký <i class="fa fa-check-circle"></i>');
                //$('#submit-seven-menu .ch-info-front h3').html('hủy đăng ký');
                $('.li-seven-menu').html('<a id="submit-seven-menu" class="del-seven-menu">' +
                '<div class="ch-item">' +
                '<div class="ch-info">' +
                '<div class="ch-info-front ch-img-2">' +
                '<h3>hủy đăng ký</h3>' +
                '</div>' +
                '<div class="ch-info-back ch-img-2">' +
                '<i class="fa fa-minus-square"></i>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>');
            }else{
                $('.alert-seven').removeClass('alert-success');
                $('.alert-seven').addClass('alert-warning');
                $('.alert-seven h4').html('Bạn chưa đăng ký <i class="fa fa-circle-o"></i>');
                $('#submit-seven-menu .ch-info-front h3').html('Đăng ký ngay');
                $('.li-seven-menu').html('<a id="submit-seven-menu" class="res-seven-menu">' +
                '<div class="ch-item">' +
                '<div class="ch-info">' +
                '<div class="ch-info-front ch-img-1">' +
                '<h3>đăng ký ngay</h3>' +
                '</div>' +
                '<div class="ch-info-back ch-img-1">' +
                '<i class="fa fa-plus-square"></i>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>');
            }

			var dir = catidx > this.current ? 'right' : 'left',
				toClass = dir === 'right' ? 'mi-moveToLeft' : 'mi-moveToRight',
				fromClass = dir === 'right' ? 'mi-moveFromRight' : 'mi-moveFromLeft',
				// current category la thang ul truoc thang se an
				$currcat = this.$categories.eq( this.current ),
				// new category
				$newcat = this.$categories.eq( catidx ),
				$newcatchild = $newcat.children(),
				lastEnter = dir === 'right' ? $newcatchild.length - 1 : 0,
				self = this;
            //console.log($currcat);

			if( this.support ) {
                //$currcat.css('display', 'none');
				$currcat.removeClass().addClass( toClass );

				
				setTimeout( function() {

					$newcat.removeClass().addClass( fromClass );
					$newcatchild.eq( lastEnter ).on( self.animEndEventName, function() {

						$( this ).off( self.animEndEventName );

						$newcat.addClass( 'mi-current' );
						self.current = catidx;
						var $this = $( this );
						// solve chrome bug
						self.forceRedraw( $this.get(0) );
						self.isAnimating = false;
                        $newcat.css('opacity', '1');
                        $currcat.css('opacity', '0');
					} );

				}, $newcatchild.length * 90 );

			}
			else {
				$currcat.hide();
				$newcat.show();
				this.current = catidx;
				this.isAnimating = false;

			}

		},
		// based on http://stackoverflow.com/a/8840703/989439
		forceRedraw : function(element) {
			if (!element) { return; }
			var n = document.createTextNode(' '),
				position = element.style.position;
			element.appendChild(n);
			element.style.position = 'relative';
			setTimeout(function(){
				element.style.position = position;
				n.parentNode.removeChild(n);
			}, 25);
		}

	}

	$.fn.catslider = function( options ) {
		var instance = $.data( this, 'catslider' );
		if ( typeof options === 'string' ) {
			var args = Array.prototype.slice.call( arguments, 1 );
			this.each(function() {
				instance[ options ].apply( instance, args );
			});
		}
		else {
			this.each(function() {
				instance ? instance._init() : instance = $.data( this, 'catslider', new $.CatSlider( options, this ) );
			});
		}
		return instance;
	};

} )( jQuery, window );