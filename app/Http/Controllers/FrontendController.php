<?php namespace App\Http\Controllers;

use App\Food;
use App\Foodmenu;
use App\Menu;
use App\User;
use App\Userday;
use App\Usermenu;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Request;
use Response;
use App\Http\Controllers\Controller;

class FrontendController extends Controller {

    public function home()
    {
        $username = \Auth::user()->name;
        $role = \Auth::user()->role;

        // For display menu next day
        $time = Carbon::now()->tomorrow()->toDateString();
//        dd($time);
        $get_menu = Menu::where('date_published' , '=' , $time);
        if( $get_menu->count() != 0)
        {
            $food_menu = Menu::where('date_published' , '=' , $time)->first()->foodmenu;

            return view('home', compact('username', 'role', 'food_menu'));
        }
        else
        {
            return view('home', compact('username', 'role'));
        }
       
    }

    public function orderMenu(){
        $username = \Auth::user()->name;
        $role = \Auth::user()->role;

        $time = Carbon::now()->tomorrow()->toDateString();
        $get_menu = Menu::where('date_published' , '=' , $time);
        if( $get_menu->count() != 0)
        {
            $food_menu = Menu::where('date_published' , '=' , $time)->first()->foodmenu;
            $time = Carbon::now()->tomorrow()->format('d/m/Y');
            return view('frontend/orderMenu', compact('username', 'role', 'time', 'food_menu'));
        }
        else
        {
            $time = Carbon::now()->tomorrow()->format('d/m/Y');
            return view('frontend/orderMenu', compact('username', 'time', 'role'));
        }


    }

    //ajax hien nut DANG KY NGAY hay la nut HUY DANG KY
    public function tomorrowMenu(){
        if (Request::ajax()) {
            $user_id = \Auth::user()->id;
            $time = Carbon::now()->tomorrow()->toDateString();

            $get_menu = Menu::where('date_published', '=', $time)->first();

            if (isset($get_menu) && $get_menu->count() != 0) {
                $check_menu = Usermenu::where('user_id', '=', $user_id)->where('menu_id', '=', $get_menu->id)->get();
                if ($check_menu->count() == 0){
                    return Response::json(array(
                        'success' => 'dangkyngay',
                    ));
                }
                else{
                    return Response::json(array(
                        'success' => 'huydangky',
                    ));
                }

            } else {
                return Response::json(array(
                    'nothing' => true,
                ));
            }
        }
    }

    public function MenuRegister($id){
        if (Request::ajax()) {
            $user_id = \Auth::user()->id;
            $time = $id;
            $get_menu = Menu::where('date_published', '=', $time)->first();

            if (isset($get_menu) && $get_menu->count() != 0) {
                $check_menu = Usermenu::where('user_id', '=', $user_id)->where('menu_id', '=', $get_menu->id)->get();
                if ($check_menu->count() == 0){
                    $user_menu = new Usermenu();
                    $user_menu->user_id = $user_id;
                    $user_menu->menu_id = $get_menu->id;
                    $user_menu->save();

                    return Response::json(array(
                        'success' => true,
                    ));
                }
                else{
                    return Response::json(array(
                        'success' => false,
                    ));
                }

            } else {
                return Response::json(array(
                    'error' => true,
                ));
            }
        }
    }

    public function MenuDelete($id){
        if (Request::ajax()) {
            $user_id = \Auth::user()->id;
            $get_menu = Menu::where('date_published', '=', $id)->first();

            if ($get_menu->count() != 0) {
                $user_menu = Usermenu::where('user_id', '=', $user_id)->where('menu_id', '=', $get_menu->id)->first();
                $user_menu->delete();

                return Response::json(array(
                    'success' => true,
                ));
            } else {
                return Response::json(array(
                    'error' => true,
                ));
            }
        }
    }

    public function sevenMenu(){
        if (Request::ajax()) {
            $user_id = \Auth::user()->id;

            //lay 7 ngay tiep theo
            $time_array = [];
            $foods_time = [];
            $cost_time = [];
            $check_seven = [];
            for ($i=1; $i<=7; $i++){
                //lay 7 ngay
                $next_time = Carbon::now()->addDays($i)->format('d/m');
                array_push($time_array, $next_time);
                $time_each = Carbon::now()->addDays($i)->format('y/m/d');
                $foods = DB::table('foods')
                    ->join('food_menu', 'foods.id', '=', 'food_menu.food_id')
                    ->join('menus', 'food_menu.menu_id', '=', 'menus.id')
                    ->where('menus.date_published', '=', $time_each)
                    ->get();
                array_push($foods_time, $foods);
                //lay menu 7 ngay
                $cost = DB::table('menus')
                    ->select('total_cost')
                    ->where('date_published', '=', $time_each)
                    ->get();
                array_push($cost_time, $cost);

                //kiem tra DANG KY chua 7 ngay
                $get_menu = Menu::where('date_published', '=', $time_each)->first();
                if (isset($get_menu) && $get_menu->count() != 0) {
                    $check_menu = Usermenu::where('user_id', '=', $user_id)->where('menu_id', '=', $get_menu->id)->get();
                    if ($check_menu->count() == 0){
                        array_push($check_seven, 'false'); //chua dang ky
                    }
                    else{
                       array_push($check_seven, 'true'); //da dang ky
                    }

                } else {
                    array_push($check_seven, 'false');
                }
            }

            return Response::json(array(
                        'timearray' => $time_array,
                        'foodstime' => $foods_time,
                        'costtime' => $cost_time,
                        'checkseven' => $check_seven,
                    ));


        }
    }

    public function fixedMenu(){
        if (Request::ajax()) {
            $user_id = \Auth::user()->id;

            $user_day = Userday::where('user_id', '=', $user_id)->get();

            return Response::json(array(
                'success' => true,
                'user_day' => $user_day,
            ));
        }
    }

    public function fixedMenuRes(){
        if (Request::ajax()) {
            $user_id = \Auth::user()->id;
            $list_day = Input::get('list_day');

            $user_day = Userday::where('user_id', '=', $user_id)->get();
            foreach($user_day as $userday){
                $userday->delete();
            }

            if (isset($list_day) && count($list_day)) {

                $day_name = ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'];
                $day_res = [];

                foreach ($list_day as $day) {
                    $user_day = new Userday();
                    $user_day->user_id = $user_id;
                    $user_day->day_id = $day;
                    $user_day->day_name = $day_name[$day - 1];
                    $user_day->save();
                    array_push($day_res, $day_name[$day - 1]);
                }

                return Response::json(array(
                    'success' => true,
                    'day_res' => $day_res,
                ));
            }else{
                return Response::json(array(
                    'success' => false,
                ));
            }
        }
    }

    /**
     *  For contact view
     */
    public function contact(){
        $username = \Auth::user()->name;
        $role = \Auth::user()->role;

        return view('contact', compact('username', 'role'));
    }

    public function costtotal(){
        $username = \Auth::user()->name;
        $role = \Auth::user()->role;
        $user_id = \Auth::user()->id;

        $months = Usermenu::select(DB::raw('month(created_at) as month'))->where('user_id', '=', $user_id)->groupby('month')->orderby('month','desc')->get();
//        dd(isset($months) && count($months));
        if (isset($months) && count($months)) {
            $list_month = [];
            foreach ($months as $month) {
                array_push($list_month, $month->month);
            }
            $lastest_month = $list_month[0];
            $top_menu = DB::table('user_menu')
                ->select(DB::raw('date(user_menu.created_at) as date'), DB::raw('time(user_menu.created_at) as time'), 'menus.total_cost', 'menus.id', 'user_menu.check_pay')
                ->join('menus', 'user_menu.menu_id', '=', 'menus.id')
                ->where('user_id', '=', $user_id)
                ->where(DB::raw('month(user_menu.created_at)'), '=', $lastest_month)
                ->get();
            $total = 0;
            foreach ($top_menu as $menu) {
                $total += $menu->total_cost;
            }

            return view('frontend.costtotal', compact('username', 'role', 'total', 'list_month', 'top_menu'));
        }else{
            $list_month=['0'];

            return view('frontend.costtotal', compact('username', 'role', 'list_month'));
        }
    }

    public function showByMonth($id){
        if (Request::ajax()) {
            $user_id = \Auth::user()->id;

            $top_menu = DB::table('user_menu')
                ->select(DB::raw('date(user_menu.created_at) as date'),DB::raw('time(user_menu.created_at) as time'), 'menus.total_cost', 'menus.id', 'user_menu.check_pay')
                ->join('menus', 'user_menu.menu_id', '=', 'menus.id')
                ->where('user_id', '=', $user_id)
                ->where(DB::raw('month(user_menu.created_at)'), '=' , $id)
                ->get();

            $total = 0;
            foreach($top_menu as $menu){
                $total += $menu->total_cost;
            }

            return Response::json(array(
                'success' => true,
                'topmenu' => $top_menu,
                'total' => $total,
            ));
        }
    }

    public function showDetails($id){
        if (Request::ajax()) {
            $food_menu = Menu::where('id' , '=' , $id)->first()->foodmenu;

            $foods = [];
            foreach ($food_menu as $food){
                array_push($foods, $food->food);
            }

            return Response::json(array(
                'success' => true,
                'foodmenu' =>$foods,
            ));
        }
    }
}
