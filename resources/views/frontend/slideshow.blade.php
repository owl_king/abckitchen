<section id="main-slide">

        <div class="row">
            <div id="jms-slideshow" class="jms-slideshow">
                <div class="step" data-color="color-2">
                    <div class="jms-content">
                        <h3>Vài nét về ABC Kitchen</h3>
                        <p>Là nhà ăn thuộc công ty ABC, công ty hàng đầu Đông Nam Á</p>
                        {{--<a class="jms-link" href="#"></a>--}}
                    </div>
                    <img src="images/1.png" />
                </div>
                <div class="step" data-color="color-3" data-y="900" data-rotate-x="80">
                    <div class="jms-content">
                        <h3>Món ăn chất lượng!</h3>
                        <p>Chúng tôi cam kết về chất lượng thức ăn cũng như những yêu cầu về an toàn vệ sinh thực phẩm</p>
                        {{--<a class="jms-link" href="#">Read more</a>--}}
                    </div>
                    <img src="images/2.png" />
                </div>
                <div class="step" data-color="color-4" data-x="-100" data-z="1500" data-rotate="170">
                    <div class="jms-content">
                        <h3>Tiện lợi, nhanh chóng!</h3>
                        <p>Bạn có thể đăng ký bữa ăn ngay ngày mai bằng 1 cú click chuột</p>
                        {{--<a class="jms-link" href="#">Read more</a>--}}
                    </div>
                    <img src="images/3.png" />
                </div>
                <div class="step" data-color="color-5" data-x="3000">
                    <div class="jms-content">
                        <h3>Hỗ trợ người lao động!</h3>
                        <p>Công ty ABC hỗ trợ 50% tiền ăn hàng tháng cho người lao động!</p>
                        {{--<a class="jms-link" href="#">Read more</a>--}}
                    </div>
                    <img src="images/4.png" />
                </div>
                <div class="step" data-color="color-1" data-x="4500" data-z="1000" data-rotate-y="45">
                    <div class="jms-content">
                        <h3>Có thể bạn chưa biết...</h3>
                        <p>Được phục vụ bữa ăn cho các bạn là niềm vinh hạnh của chúng tôi!</p>
                        {{--<a class="jms-link" href="#">Read more</a>--}}
                    </div>
                    <img src="images/5.png" />
                </div>
            </div>
        </div>
</section>

