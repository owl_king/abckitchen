<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Userday extends Model {

    protected $table = 'user_day';

    protected $fillable =['user_id', 'day_id', 'day_name'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
