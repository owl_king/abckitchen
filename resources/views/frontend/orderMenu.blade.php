@extends('...frontend')

@section('title')
    Đăng ký món
@stop

@section('content')
    <section class="section-order">
        <ul class="ch-grid">
            <li>
                <a href="#tomorrow-menu-order">
                    <div class="ch-item">
                        <div class="ch-info">
                            <div class="ch-info-front ch-img-1">
                                <h3>Ngày mai</h3>
                                <p>Đăng ký</p>
                            </div>
                            <div class="ch-info-back ch-img-1">
                                <h4>Ngày mai</h4>
                                <p>Bạn có thể xem và đăng ký nhanh suất ăn cho ngày mai</p>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#seven-menu">
                    <div class="ch-item">
                        <div class="ch-info">
                            <div class="ch-info-front ch-img-2">
                                <h3>7 ngày tới</h3>
                                <p>Đăng ký</p>
                            </div>
                            <div class="ch-info-back ch-img-2">
                                <h4>7 ngày tới</h4>
                                <p>Bạn có thể xem và đăng ký suất ăn cho nhiều ngày trong 7 ngày tiếp theo</p>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#fixed-menu">
                    <div class="ch-item">
                        <div class="ch-info">
                            <div class="ch-info-front ch-img-3">
                                <h3>Ngày cố định</h3>
                                <p>Đăng ký</p>
                            </div>
                            <div class="ch-info-back ch-img-3">
                                <h4>Ngày cố định</h4>
                                <p>Bạn có thể đăng ký suất ăn vào một số ngày cố định hàng tuần</p>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
        </ul>
    </section>
<div class="container-div">


    <div class="row">
        <label class="lable-tmr">
            <h2>
                <a id="tomorrow-menu-order" class="tomorrow-seven-fixed">
                    Thực đơn ngày mai
                </a>
            </h2>
            <div class="day-show-tmr"><i class="fa fa-calendar"></i> {{ $time }}</div>
        </label>
        @include('frontend/tomorrowMenu')
    </div>
    <hr>
    <div class="row">
        <label class="lable-tmr">
            <h2>
                <a id="seven-menu" class="tomorrow-seven-fixed">
                    Thực đơn 7 ngày tới
                </a>
            </h2>
        </label>
        @include('frontend/sevenMenu')
    </div>
    <hr>
    <div class="row">
        <label class="lable-tmr">
            <h2>
                <a id="fixed-menu" class="tomorrow-seven-fixed">
                    Đăng ký ngày cố định
                </a>
            </h2>
        </label>
        @include('frontend/fixedMenu')
    </div>
</div>
@stop