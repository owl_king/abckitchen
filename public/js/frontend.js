function run_mi_slider()  {
    $( '#mi-slider' ).catslider();
}

//config slide homepage
function run_slick_slide_mi_slider(){
    $('.each-seven-day').slick({
        slidesToShow: 4,
        prevArrow: '<button type="button" class="slick-prev pre">Previous</button>',
        nextArrow: '<button type="button" class="slick-next next">Next</button>',
        autoplay: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {

                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerPadding: '0px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            }
        ]
    });
}

//slideshow
$(function() {

    var jmpressOpts	= {
        animation		: { transitionDuration : '1.0s' }
    };

    $( '#jms-slideshow' ).jmslideshow( $.extend( true, { jmpressOpts : jmpressOpts }, {
        autoplay	: true,
        bgColorSpeed: '1.0s',
        arrows		: true
    }));
});

// config slide homepage
$(document).ready(function(){
    $('.list-food').slick({
        slidesToShow: 4,
        prevArrow: '<button type="button" class="slick-prev pre">Previous</button>',
        nextArrow: '<button type="button" class="slick-next next">Next</button>',
        autoplay: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {

                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerPadding: '0px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            }
        ]
    });
});

$(document).ready(function() {
    var URL = window.location.href;
    if (typeof location.origin === 'undefined')
        location.origin = location.protocol + '//' + location.host;
    var form = location.origin + '/ordermenu';
    //console.log(URL.indexOf(form));
    if (URL.indexOf(form) > -1) {

        //phan xu ly cho phan Thuc don ngay mai
        var formURL = location.origin;
        formURL += '/ordermenu/tomorrowmenu';

        //console.log(formURL);

        $.ajax({
            url: formURL,
            type: 'post',
            data: {_token: $('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success: function (data) {
                //console.log("ajax sent success");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function (data) {
                if (data.success == 'dangkyngay'){
                    //console.log('dangkyngay');
                    //neu nguoi dung chua dang ky ngay mai thi se hien nut DANG KY NGAY va hien trang thai BAN CHUA DANG KY
                    $('.li-tomorrow-menu').append('<a id="submit-tomorrow-menu" class="res-tomorrow-menu">' +
                    '<div class="ch-item"><div class="ch-info">' +
                    '<div class="ch-info-front ch-img-1"><h3>Đăng ký ngay</h3></div>' +
                    '<div class="ch-info-back ch-img-1">' +
                    '<i class="fa fa-plus-square"></i></div></div></div></a>'
                    );

                    $('.order-tomorrow-state').append('<div class="alert alert-warning" role="alert"><h4>Bạn chưa đăng ký <i class="fa fa-circle-o"></i></h4></div>');
                }else
                    if(data.success == 'huydangky'){
                        //console.log('huydangky');
                        //neu nguoi dung da dang ky ngay mai thi se hien nut HUY DANG KY va hien trang thai BAN DA DANG KY
                        $('.li-tomorrow-menu').append('<a id="submit-tomorrow-menu" class="del-tomorrow-menu">' +
                        '<div class="ch-item"><div class="ch-info">' +
                        '<div class="ch-info-front ch-img-2"><h3>Hủy<br>đăng ký</h3></div>' +
                        '<div class="ch-info-back ch-img-2">' +
                        '<i class="fa fa-minus-square"></i></div></div></div></a>'
                        );

                        $('.order-tomorrow-state').append('<div class="alert alert-success" role="alert"><h4>Bạn đã đăng ký <i class="fa fa-check-circle"></i></h4></div>');
                    }
                if (data.nothing){
                    $('.tomorrow-menu-show').height(100);
                    $('.tinhtrang').height(100);
                }
            })
            .error(function (data) {
                console.log('error');
                swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
            });

        //xu ly cho phan Thuc don 7 ngay tiep theo
        var formURLseven = location.origin;
        formURLseven += '/ordermenu/sevenmenu';

        $.ajax({
            url: formURLseven,
            type: 'post',
            data: {_token: $('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success: function (data) {
                //console.log("ajax sent success");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function (data) {
                //console.log(data.timearray);
                $('.mi-slider nav a').each(function (index) {
                    $(this).html(data.timearray[index]);
                });

                $('.day-show-seven').html('<i class="fa fa-calendar"></i> '+data.timearray[0]);

                // Hien slide tab THUC DON 7 NGAY
                var ul_each_slide = "";
                for (var i=0; i<7; i++){
                    ul_each_slide += '<ul><div class="each-seven-day">';
                    if (data.foodstime[i].length == 0){
                        ul_each_slide+='<li class="alert-mi-slide"><a><h3>'+'Chưa có thực đơn cho ngày này'+'</h3></a></li>';
                    }else
                        for(var j=0; j<data.foodstime[i].length; j++){
                            ul_each_slide += '<li><a><img src="'+ data.foodstime[i][j]["img"]+'" alt="Hình '+(j+1)+'" ><h4>'+data.foodstime[i][j]["name"].capitalize()+'</h4></a></li>';
                        }
                    ul_each_slide+= '</div></ul>';

                    $('.mi-slider nav a').each(function (index) {
                        if (i == index){
                            if (typeof data.costtime[i][0] ==='undefined'){
                                $(this).attr('total_cost', '0');
                            }
                            else{
                                $(this).attr('total_cost', data.costtime[i][0]['total_cost']);
                            }
                            $(this).attr('check_reg', data.checkseven[i]);
                        }
                    });
                }

                //hien tong gia ngay tu luc dau cua ngay dau tien
                if (typeof data.costtime[0][0] !=='undefined'){
                    $('.label-seven-menu #cost-seven-menu').html(data.costtime[0][0]['total_cost']);
                }
                //hien trang thai dau tien
                if ($('.mi-slider nav a').first().attr('check_reg')=='true'){
                    $('.alert-seven').removeClass('alert-warning');
                    $('.alert-seven').addClass('alert-success');
                    $('.alert-seven h4').html('Bạn đã đăng ký <i class="fa fa-check-circle"></i>');
                    $('.li-seven-menu').html('<a id="submit-seven-menu" class="del-seven-menu">' +
                    '<div class="ch-item">' +
                    '<div class="ch-info">' +
                    '<div class="ch-info-front ch-img-2">' +
                    '<h3>hủy đăng ký</h3>' +
                    '</div>' +
                    '<div class="ch-info-back ch-img-2">' +
                    '<i class="fa fa-minus-square"></i>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</a>');
                }else{
                    $('.alert-seven').removeClass('alert-success');
                    $('.alert-seven').addClass('alert-warning');
                    $('.alert-seven h4').html('Bạn chưa đăng ký <i class="fa fa-circle-o"></i>');
                    $('.li-seven-menu').html('<a id="submit-seven-menu" class="res-seven-menu">' +
                    '<div class="ch-item">' +
                    '<div class="ch-info">' +
                    '<div class="ch-info-front ch-img-1">' +
                    '<h3>đăng ký ngay</h3>' +
                    '</div>' +
                    '<div class="ch-info-back ch-img-1">' +
                    '<i class="fa fa-plus-square"></i>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</a>');
                }

                $('.mi-slider').prepend(ul_each_slide);
                run_mi_slider();
                run_slick_slide_mi_slider();
            })
            .error(function (data) {
                console.log('error');
                swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
            });

        //xu ly phan DANG KY CO DINH
        var formURLseven = location.origin;
        formURLseven += '/ordermenu/fixedmenu';

        $.ajax({
            url: formURLseven,
            type: 'post',
            data: {_token: $('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success: function (data) {
                //console.log("ajax sent success");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function (data) {
                if (data.success){
                    //console.log(data.user_day);
                    for (var i=0; i<data.user_day.length; i++) {
                        $('.fixed-menu ul li').each(function (index) {
                            if ((index+1) == data.user_day[i]['day_id']) {
                                //console.log(index);
                                $(this).find('input').attr('checked', true);
                                $(this).addClass('active');
                            }
                        });
                    }
                }
            })
            .error(function (data) {
                console.log('error');
                swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
            });
    }
});

//khi click DANH KY NGAY o NGAY MAI menu
$(document).ready(function() {
    $(document).on('click', "a.res-tomorrow-menu", function (event) {
        event.preventDefault();
        //console.log('da an tomorrow');
        swal({
                title: "Bạn muốn đăng ký ăn ngày mai?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonText: "Đồng ý",
                confirmButtonColor: "#5CB85C",
                closeOnConfirm: false
            },
            function(){
                if (typeof location.origin === 'undefined')
                    location.origin = location.protocol + '//' + location.host;
                var formURL = location.origin;
                var days = 1;
                var date = new Date();
                var res = date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var date = new Date(res);
                var day = date.getDate();
                var mon = date.getMonth()+1;
                if (mon < 10) mon = '0' + (date.getMonth()+1);
                var yea= date.getFullYear();
                var final_day = yea+'-'+mon+'-'+day;
                //console.log(final_day);

                formURL += '/ordermenu/menuregister/'+final_day;

                console.log(formURL);

                $.ajax({
                    url: formURL,
                    type:'post',
                    data: { _token:$('meta[name="_token"]').val()},
                    dataType: 'json',
                    cache: false,
                    success : function(data){
                        console.log("ajax sent success");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                        console.log(errorThrown);
                    }
                })
                    .done(function(data){
                        if (data.success){
                            console.log('da tao usermenu thanh cong');
                            //alert('Bạn đã đăng ký khẩu phần ăn ngày mai thành công!')
                            swal("Thành công!", "Bạn đã đăng ký thành công khẩu phần ăn ngày mai!", "success");

                            $('.li-tomorrow-menu').html("");
                            $('.li-tomorrow-menu').append('<a id="submit-tomorrow-menu" class="del-tomorrow-menu">' +
                                '<div class="ch-item"><div class="ch-info">' +
                                '<div class="ch-info-front ch-img-2"><h3>Hủy<br>đăng ký</h3></div>' +
                                '<div class="ch-info-back ch-img-2">' +
                                '<i class="fa fa-minus-square"></i></div></div></div></a>'
                            );

                            $('.order-tomorrow-state').html("");
                            $('.order-tomorrow-state').append('<div class="alert alert-success" role="alert"><h4>Bạn đã đăng ký <i class="fa fa-check-circle"></i></h4></div>');

                            $('.mi-slider nav a').first().removeAttr('check_reg');
                            $('.mi-slider nav a').first().attr('check_reg', 'true');

                            //sua lai phan 7 neu phan mai thay doi
                            if ($('.mi-slider nav a').first().hasClass('mi-selected')){
                                $('.li-seven-menu').html("");
                                $('.li-seven-menu').append('<a id="submit-seven-menu" class="del-seven-menu">' +
                                    '<div class="ch-item"><div class="ch-info">' +
                                    '<div class="ch-info-front ch-img-2"><h3>hủy đăng ký</h3></div>' +
                                    '<div class="ch-info-back ch-img-2">' +
                                    '<i class="fa fa-minus-square"></i></div></div></div></a>'
                                );

                                $('.alert-seven').removeClass('alert-warning');
                                $('.alert-seven').addClass('alert-success');
                                $('.alert-seven h4').html('Bạn đã đăng ký <i class="fa fa-check-circle"></i>');
                            }
                        }
                    })
                    .error(function (data) {
                        console.log('error');
                        swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
                    });
            });

    });
});

//khi click HUY DANG KY o NGAY MAI menu
$(document).ready(function() {
    $(document).on('click', "a.del-tomorrow-menu", function (event) {
        event.preventDefault();
        //console.log('da an tomorrow');
        swal({
                title: "Bạn muốn hủy đăng ký ăn ngày mai?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonText: "Đồng ý",
                confirmButtonColor: "#5CB85C",
                closeOnConfirm: false
            },
            function(){
                if (typeof location.origin === 'undefined')
                    location.origin = location.protocol + '//' + location.host;
                var formURL = location.origin;

                var days = 1;
                var date = new Date();
                var res = date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var date = new Date(res);
                var day = date.getDate();
                var mon = date.getMonth()+1;
                if (mon < 10) mon = '0' + (date.getMonth()+1);
                var yea= date.getFullYear();
                var final_day = yea+'-'+mon+'-'+day;

                formURL += '/ordermenu/menudelete/'+final_day;

                $.ajax({
                    url: formURL,
                    type:'post',
                    data: { _token:$('meta[name="_token"]').val()},
                    dataType: 'json',
                    cache: false,
                    success : function(data){
                        console.log("ajax sent success");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                        console.log(errorThrown);
                    }
                })
                    .done(function(data){
                        if (data.success){
                            console.log('da xoa usermenu thanh cong');
                            //alert('Bạn đã đăng ký khẩu phần ăn ngày mai thành công!')
                            swal("Hủy thành công!", "Bạn đã hủy thành công đăng ký khẩu phần ăn ngày mai!", "success");
                            //sua lai noi dung nut chinh
                            $('.li-tomorrow-menu').html("");
                            $('.li-tomorrow-menu').append('<a id="submit-tomorrow-menu" class="res-tomorrow-menu">' +
                                '<div class="ch-item"><div class="ch-info">' +
                                '<div class="ch-info-front ch-img-1"><h3>Đăng ký ngay</h3></div>' +
                                '<div class="ch-info-back ch-img-1">' +
                                '<i class="fa fa-plus-square"></i></div></div></div></a>'
                            );
                            //sua lai o trang thai
                            $('.order-tomorrow-state').html("");
                            $('.order-tomorrow-state').append('<div class="alert alert-warning" role="alert"><h4>Bạn chưa đăng ký <i class="fa fa-circle-o"></i></h4></div>');
                            //sua lai check_reg cua nav
                            $('.mi-slider nav a').first().removeAttr('check_reg');
                            $('.mi-slider nav a').first().attr('check_reg', 'false');
                            //sua lai phan 7 neu phan mai thay doi
                            if ($('.mi-slider nav a').first().hasClass('mi-selected')){
                                $('.li-seven-menu').html("");
                                $('.li-seven-menu').append('<a id="submit-seven-menu" class="res-seven-menu">' +
                                    '<div class="ch-item"><div class="ch-info">' +
                                    '<div class="ch-info-front ch-img-1"><h3>đăng ký ngay</h3></div>' +
                                    '<div class="ch-info-back ch-img-1">' +
                                    '<i class="fa fa-plus-square"></i></div></div></div></a>'
                                );

                                $('.alert-seven').removeClass('alert-success');
                                $('.alert-seven').addClass('alert-warning');
                                $('.alert-seven h4').html('Bạn chưa đăng ký <i class="fa fa-circle-o"></i>');
                            }

                        }
                    })
                    .error(function (data) {
                        console.log('error');
                        swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
                    });
            });

    });
});


$(function () {
    if ($('.tomorrow-menu-show').height()>$('.tinhtrang').height()){
        $('.tinhtrang').height($('.tomorrow-menu-show').height());
    }
})


//khi an nut DANG KY NGAY o menu 7 ngay
$(document).ready(function() {
    $(document).on('click', "a.res-seven-menu", function (event) {
        event.preventDefault();
        //console.log('da an tomorrow');
        swal({
                title: "Bạn muốn đăng ký ăn ngày này?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonText: "Đồng ý",
                confirmButtonColor: "#5CB85C",
                closeOnConfirm: false
            },
            function(){
                if (typeof location.origin === 'undefined')
                    location.origin = location.protocol + '//' + location.host;
                var formURL = location.origin;

                var days = 0;
                $('.mi-slider nav a').each(function (index) {
                    if ($(this).hasClass('mi-selected')){
                        days = index+1;
                    }
                });

                var date = new Date();
                var res = date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var date = new Date(res);
                var day = date.getDate();
                var mon = date.getMonth()+1;
                if (mon < 10) mon = '0' + (date.getMonth()+1);
                var yea= date.getFullYear();
                var final_day = yea+'-'+mon+'-'+day;

                formURL += '/ordermenu/menuregister/'+final_day;

                $.ajax({
                    url: formURL,
                    type:'post',
                    data: { _token:$('meta[name="_token"]').val()},
                    dataType: 'json',
                    cache: false,
                    success : function(data){
                        console.log("ajax sent success");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                        console.log(errorThrown);
                    }
                })
                    .done(function(data){
                        if (data.success){
                            swal("Thành công!", "Bạn đã đăng ký thành công khẩu phần ăn ngày này!", "success");
                            $('.li-seven-menu').html("");
                            $('.li-seven-menu').append('<a id="submit-seven-menu" class="del-seven-menu">' +
                                '<div class="ch-item"><div class="ch-info">' +
                                '<div class="ch-info-front ch-img-2"><h3>Hủy đăng ký</h3></div>' +
                                '<div class="ch-info-back ch-img-2">' +
                                '<i class="fa fa-minus-square"></i></div></div></div></a>'
                            );

                            $('.alert-seven').removeClass('alert-warning');
                            $('.alert-seven').addClass('alert-success');
                            $('.alert-seven h4').html('Bạn đã đăng ký <i class="fa fa-check-circle"></i>');

                            $('.mi-slider nav a.mi-selected').removeAttr('check_reg');
                            $('.mi-slider nav a.mi-selected').attr('check_reg', 'true');

                            if ($('.mi-slider nav a').first().hasClass('mi-selected')) {
                                $('.li-tomorrow-menu').html("");
                                $('.li-tomorrow-menu').append('<a id="submit-tomorrow-menu" class="del-tomorrow-menu">' +
                                    '<div class="ch-item"><div class="ch-info">' +
                                    '<div class="ch-info-front ch-img-2"><h3>Hủy<br>đăng ký</h3></div>' +
                                    '<div class="ch-info-back ch-img-2">' +
                                    '<i class="fa fa-minus-square"></i></div></div></div></a>'
                                );

                                $('.order-tomorrow-state').html("");
                                $('.order-tomorrow-state').append('<div class="alert alert-success" role="alert"><h4>Bạn đã đăng ký <i class="fa fa-check-circle"></i></h4></div>');
                            }
                        }
                        if (data.error){
                            swal("Không thành công!", "Chưa có thực đơn cho ngày này!", "warning");
                        }
                    })
                    .error(function (data) {
                        console.log('error');
                        swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
                    });
            });

    });
});

//khi click HUY DANG KY o 7 NGAY menu
$(document).ready(function() {
    $(document).on('click', "a.del-seven-menu", function (event) {
        event.preventDefault();
        //console.log('da an tomorrow');
        swal({
                title: "Bạn muốn hủy đăng ký ăn ngày mai?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonText: "Đồng ý",
                confirmButtonColor: "#5CB85C",
                closeOnConfirm: false
            },
            function(){
                if (typeof location.origin === 'undefined')
                    location.origin = location.protocol + '//' + location.host;
                var formURL = location.origin;

                var days = 0;
                $('.mi-slider nav a').each(function (index) {
                    if ($(this).hasClass('mi-selected')){
                        days = index+1;
                    }
                });

                var date = new Date();
                var res = date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var date = new Date(res);
                var day = date.getDate();
                var mon = date.getMonth()+1;
                if (mon < 10) mon = '0' + (date.getMonth()+1);
                var yea= date.getFullYear();
                var final_day = yea+'-'+mon+'-'+day;

                formURL += '/ordermenu/menudelete/'+final_day;

                $.ajax({
                    url: formURL,
                    type:'post',
                    data: { _token:$('meta[name="_token"]').val()},
                    dataType: 'json',
                    cache: false,
                    success : function(data){
                        console.log("ajax sent success");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                        console.log(errorThrown);
                    }
                })
                    .done(function(data){
                        if (data.success){
                            swal("Hủy thành công!", "Bạn đã hủy thành công đăng ký khẩu phần ăn ngày mai!", "success");

                            $('.li-seven-menu').html("");
                            $('.li-seven-menu').append('<a id="submit-seven-menu" class="res-seven-menu">' +
                                '<div class="ch-item"><div class="ch-info">' +
                                '<div class="ch-info-front ch-img-1"><h3>đăng ký ngay</h3></div>' +
                                '<div class="ch-info-back ch-img-1">' +
                                '<i class="fa fa-plus-square"></i></div></div></div></a>'
                            );

                            $('.alert-seven').removeClass('alert-success');
                            $('.alert-seven').addClass('alert-warning');
                            $('.alert-seven h4').html('Bạn chưa đăng ký <i class="fa fa-circle-o"></i>');

                            $('.mi-slider nav a.mi-selected').removeAttr('check_reg');
                            $('.mi-slider nav a.mi-selected').attr('check_reg', 'false');

                            if ($('.mi-slider nav a').first().hasClass('mi-selected')) {
                                $('.li-tomorrow-menu').html("");
                                $('.li-tomorrow-menu').append('<a id="submit-tomorrow-menu" class="res-tomorrow-menu">' +
                                    '<div class="ch-item"><div class="ch-info">' +
                                    '<div class="ch-info-front ch-img-1"><h3>đăng ký ngay</h3></div>' +
                                    '<div class="ch-info-back ch-img-1">' +
                                    '<i class="fa fa-plus-square"></i></div></div></div></a>'
                                );

                                $('.order-tomorrow-state').html("");
                                $('.order-tomorrow-state').append('<div class="alert alert-warning" role="alert"><h4>Bạn chưa đăng ký <i class="fa fa-circle-o"></i></h4></div>');
                            }
                        }
                    })
                    .error(function (data) {
                        console.log('error');
                        swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
                    });
            });

    });
});

//dang ky o muc CO DINH
$(function () {
    $('.fixed-menu ul li').each(function (index) {
        ($(this).find('input')).click(function () {
            //console.log($(this));
            if ($(this).is(':checked')) {
                $(this).parent().removeClass('active').addClass('active');
            }else{
                $(this).parent().removeClass('active');
            }
        });

    });


});

$(function () {
    $('#submit-fixed-menu').on('click', function (event) {
        event.preventDefault();

        var list_day = [];
        $('.fixed-menu ul li').each(function (index) {
            if ($(this).find('input').is(':checked')) {
                //console.log(index);
                list_day.push(index+1);
            }
        });

        if (list_day.length == 0){
            swal({
                    title: "Bạn không chọn ngày nào cả?",
                    text: "",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonText: "Đồng ý",
                    confirmButtonColor: "#5CB85C",
                    closeOnConfirm: false
                },
                function() {
                    if (typeof location.origin === 'undefined')
                        location.origin = location.protocol + '//' + location.host;
                    var formURL = location.origin;
                    formURL += '/ordermenu/fixedmenures';
                    console.log(formURL);
                    $.ajax({
                        url: formURL,
                        type: 'post',
                        data: {list_day: list_day, _token: $('meta[name="_token"]').val()},
                        dataType: 'json',
                        cache: false,
                        success: function (data) {
                            console.log("ajax sent success");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                            console.log(errorThrown);
                        }
                    })
                        .done(function (data) {
                            if (data.success){
                                var str_day = '';
                                for (var i=0; i<data.day_res.length; i++){
                                    str_day += data.day_res[i]+ ', ';
                                }
                                swal("Đăng ký thành công!", str_day+"đã được đăng ký thành công!", "success");
                            }else{
                                swal("Hoàn thành!","Bạn hiện tại không đăng ký cố định ngày nào!", "success");
                            }
                        })
                        .error(function (data) {
                            console.log('error');
                            swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
                        });
                });

        }else{
            swal({
                    title: "Đăng ký ăn cố định vào các ngày này?",
                    text: "",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonText: "Đồng ý",
                    confirmButtonColor: "#5CB85C",
                    closeOnConfirm: false
                },
                function() {
                    if (typeof location.origin === 'undefined')
                        location.origin = location.protocol + '//' + location.host;
                    var formURL = location.origin;
                    formURL += '/ordermenu/fixedmenures';
                    console.log(formURL);
                    $.ajax({
                        url: formURL,
                        type: 'post',
                        data: {list_day: list_day, _token: $('meta[name="_token"]').val()},
                        dataType: 'json',
                        cache: false,
                        success: function (data) {
                            console.log("ajax sent success");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                            console.log(errorThrown);
                        }
                    })
                        .done(function (data) {
                            if (data.success){
                                var str_day = '';
                                for (var i=0; i<data.day_res.length; i++){
                                    str_day += data.day_res[i]+ ', ';
                                }
                                swal("Đăng ký thành công!", str_day+"đã được đăng ký thành công!", "success");
                            }else{
                                swal("Hoàn thành!","Bạn hiện tại không đăng ký cố định ngày nào!", "success");
                            }
                        })
                        .error(function (data) {
                            console.log('error');
                            swal("Có chút lỗi!", "Hệ thống đang lỗi chút, phiền bạn quay lại sau!", "warning");
                        });
                });
        }

    })
});

//Xy ly khi CHON THANG
$(function () {
    //$(document).on('click', ".selecte-month option", function (event) {
    //    event.preventDefault();
    $('.selecte-month').change(function(){
        //console.log($(this).html());
        var input_month = $(this).val();

        var formURL = window.location.href;
        formURL = formURL + '/showbymonth/' + input_month;
        //console.log(formURL);

        $.ajax({
            url: formURL,
            type:'post',
            data: { _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function(data){
                if(data.success){
                    //console.log(data.topmenu);
                    $('.cost-total-show tbody tr:not(".total")').remove();

                    var str_list = '';
                    for (var i=0; i<data.topmenu.length; i++){
                        str_list += '<tr>'+
                            '<td><span>'+data.topmenu[i]["date"]+'</span></td>'+
                            '<td class="time-show"><span>'+data.topmenu[i]["time"]+'</span></td>'+
                            '<td class="price">'+data.topmenu[i]["total_cost"]+'</td>'+
                            '<td class="details"><a id="'+ data.topmenu[i]["id"] +'" href="#" data-toggle="modal" data-target="#detailsModal">Xem chi tiết</a></td>'+
                        '</tr>'
                    }

                    $('.cost-total-show tbody').prepend(str_list);

                    $('.cost-total-show tbody .total .price').html(data.total);

                    if (data.topmenu[0]['check_pay']){
                        $('.alert-check-pay').removeClass('alert-warning').addClass('alert-success');
                        $('.alert-check-pay').html('<h4>Bạn đã thanh toán <i class="fa fa-check-circle"></i></h4>');
                    }else{
                        $('.alert-check-pay').removeClass('alert-success').addClass('alert-warning');
                        $('.alert-check-pay').html('<h4>Bạn chưa thanh toán <i class="fa fa-circle-o"></i></h4>');
                    }
                }
            })
            .error(function (data) {
                console.log('error');
            });
    });

});

//Xu ly khi an XEM CHI TIET
$(function () {
    $(document).on('click', ".details a", function (event) {
        event.preventDefault();
        //console.log($(this));
        var menu_id = $(this).attr('id');

        var formURL = window.location.href;
        formURL = formURL + '/showdetails/' + menu_id;
        //console.log(formURL);

        $.ajax({
            url: formURL,
            type:'post',
            data: { _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function(data){
                if(data.success){
                    console.log(data.foodmenu);
                    var str_food = '';
                    $('.show-details-food tbody').html('');
                    for (var i=0; i<data.foodmenu.length; i++){
                        str_food += '<tr>' +
                        '<td><span>'+ data.foodmenu[i]['name'] +'</span></td>'+
                        '<td class="price">'+ data.foodmenu[i]['cost'] +'</td>'+
                        '</tr>';
                    }
                    $('.show-details-food tbody').html(str_food);
                }
            })
            .error(function (data) {
                console.log('error');
            });
    });
});

$(function () {

});


$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

});