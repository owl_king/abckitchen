String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

$(function(){
    var cal = $(".calendar").calendar({
        multiSelect: false,
        getDates: function(data){
            var r = "", out = $("#calendar-output").html("");
            $.each(data, function(i, d){
               r += d;
            });
            out.html(r);

        },
        click: function(d){

            $("#container-show").css('opacity', '1');
            //gui ajax lay thong tin menu cua ngay dang chon
            var day = $('#calendar-output').html();
            var formURL = window.location.href;
            formURL += "/showMenu/"+day;
            //console.log(formURL);
            $.ajax({
                url: formURL,
                type:'post',
                data: {_token:$('meta[name="_token"]').val()},
                dataType: 'json',
                cache: false,
                success : function(data){
                    console.log("ajax sent success");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                    console.log(errorThrown);
                }
            })
                .done(function(data){
                    //console.log(data.listIdFood);
                    var ul_list = $('.list-menu-show');
                    ul_list.html("");
                    var note_menu = $('.note-menu');
                    note_menu.html("");
                    var length = data.listIdFood.length;
                    var total_cost = 0;
                    if (length == 0){
                        ul_list.append("<p class='p-list-food'>Chưa lên thực đơn cho ngày này</p>");
                        note_menu.append("<p class='p-list-food'>Không có ghi chú</p>");
                        $('#edit-menu').addClass('disabled');
                        $('#delete-menu').addClass('disabled');
                        $('#create-menu').removeClass('disabled');
                    }else {
                        $('#create-menu').addClass('disabled');
                        $('#edit-menu').removeClass('disabled');
                        $('#delete-menu').removeClass('disabled');
                        if (data.listIdFood[0]['note']==""){
                            note_menu.append("<p class='p-list-food'>Không có ghi chú</p>");
                        }else
                            note_menu.append("<p class='p-list-food'>"+data.listIdFood[0]['note']+"</p>")

                        for (var i = 0; i < length; i++) {
                            var li_list = '<li id="'+data.listIdFood[i]['food_id']+'" cost="'+data.listIdFood[i]['cost']+'">' + (data.listIdFood[i]['name']).capitalize() + '</li>'
                            ul_list.append(li_list);
                            total_cost += data.listIdFood[i]['cost'];
                        }
                    }
                    //set height
                    var height_container = $('#container-show .table').height();
                    var height_calendar = $('.calendar').height();
                    //console.log(height_container);
                    if (height_calendar<height_container){
                        $('.calendar').height(height_container);
                    }else{
                        $('.calendar').height(375);
                    }

                    $('#container-show .tongtien').html(total_cost);
                    $('#container-show .soluongdat').html(data.count_khauphanhomnay);
                })
                .error(function (data) {
                    console.log('error');
                });

        }
    });
});

//show nhung ngay da tao thuc don
$(function() {
    var loadurl = window.location.href;
    if (loadurl == 'http://abckitchen.com/admin/menus') {
        loadurl += '/showCalendar';
        $.getJSON(loadurl, function (data) {
            if (data.length != 0){
                for (var i=0; i<data.length; i++){
                    var result = $('.calendar').calendar('findDay', data[i]['date_published']);
                }
            }
        });
    }
});



//highlight menu nav
$(document).ready(function(){
    var str=window.location.href.toLowerCase();
    if (typeof location.origin === 'undefined')
        location.origin = location.protocol + '//' + location.host;
    var loadurl = location.origin;

    $("#nav > li > a").each(function() {
        //console.log(this.href.toLowerCase());
        if (str.indexOf(this.href.toLowerCase()) > -1) {
            $("li.actived-menu").removeClass("actived-menu");
            $(this).parent().addClass("actived-menu");
            console.log('bbb');
            if (str.indexOf(loadurl+'/admin/users/showmanagers') > -1 || str.indexOf(loadurl+'/admin/users/showmembers') > -1){
                $("li.actived-menu").removeClass("actived-menu");
                $('#nav #nav-4').addClass("actived-menu");
                //console.log('aaaa');
            }
        }
    });

});


$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

});

