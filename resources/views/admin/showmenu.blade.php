@extends('...backend')

@section('title')
    Quản lý thực đơn
@stop

@section('content')
    <div style="min-height: 342px;" id="page-wrapper">
    <div class="row">
    <ol class="breadcrumb">
        <li class="active">
            <i class="fa fa-calendar-o"></i> LÊN LỊCH THỰC ĐƠN
        </li>
    </ol>
    {{--<div class="col-md-4 title-search"><h2 style="color:#77BC1B; font-weight: bold"><i class="fa fa-users"></i>DANH SÁCH QUẢN TRỊ VIÊN</h2></div>--}}
    </div>
        <div class="metro">
    		<div class="group">
    			<div class="calendar small calendar-admin" data-other-days="1" data-week-start="1" data-locale='vi'></div>

    			<div id="container-show">
    				<table class="table">
                        <tbody>
                            <tr class="tr-group">
                                <th colspan="3" class="title-table title-date-show">Ngày:&nbsp;<strong id="calendar-output"></strong>
                                </th>
                            </tr>
                            <tr class="tr-group">
                                <td class="title-table">Thực đơn</td>
                                <td class="title-table ul-list">
                                    <ul class="list-menu-show"></ul>
                                </td>
                                <td rowspan="4">
                                    <div class="button-group">
                                        <button type="button" class="btn btn-success create-edit-btn disabled" id="create-menu" data-toggle="modal" data-target="#show-create-menu">Tạo thực đơn</button><br>
                                        <button type="button" class="btn btn-primary create-edit-btn" id="edit-menu" data-toggle="modal" data-target="#show-edit-menu">Sửa thực đơn</button><br>
                                        <button type="button" class="btn btn-danger delete-menu-btn" id="delete-menu">Xóa thực đơn</button>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-group">
                                <td class="title-table">Tổng tiền</td>
                                <td class="title-table tongtien">0</td>
                            </tr>
                            <tr class="tr-group">
                                <td class="title-table">Ghi chú</td>
                                <td class="title-table note-menu"><p class="p-list-food"></p></td>
                            </tr>

                            <tr class="tr-group">
                                <td class="title-table">Số lượng đã đặt</td>
                                <td class="title-table soluongdat">0</td>
                            </tr>
                        </tbody>
    				</table>
    			</div>
    		</div>
    	</div>

    	<div class="modal fade" id="show-create-menu" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title modal-title-menu">Tạo thực đơn mới</h4>
              </div>
              <div class="modal-body">
                  <div class="show-list-food-menu"></div>
                  {{--<div class="alert alert-block alert-success alear-menu">--}}
                  {{--</div>--}}
                  <div class="show-cost-food-menu">
                      <div class="alert alert-block alert-success alert-total-cost">Tổng tiền:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>0</strong></div>
                  </div>
                <form class="form-menu" action="{{ url('/admin/menus/createmenu') }}" method="POST">
                  <div class="form-group">
                    <label for="recipient-name" class="control-label">Món ăn:</label>
                    <input type="text" class="form-control food-menu-name" placeholder="Thêm món ăn">
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="control-label">Ghi chú:</label>
                    <textarea class="form-control message-text-note" placeholder="Viết ghi chú"></textarea>
                  </div>
                    <div class="modal-footer">
                        <button type="submit" type="button" id="create-new-menu" class="btn btn-primary">Tạo mới</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>

                    </div>
                </form>
              </div>

            </div>
          </div>
        </div>

        <div class="modal fade" id="show-edit-menu" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title modal-title-menu">Chỉnh sửa thực đơn</h4>
                    </div>
                    <div class="modal-body">
                        <div class="show-list-food-menu"></div>
                        {{--<div class="alert alert-block alert-success alear-menu">--}}
                        {{--</div>--}}
                        <div class="show-cost-food-menu">
                            <div class="alert alert-block alert-success alert-total-cost">Tổng tiền:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>0</strong></div>
                        </div>
                        <form class="form-menu-edit" action="{{ url('/admin/menus/editmenu') }}" method="POST">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Món ăn:</label>
                                <input type="text" class="form-control food-menu-name" placeholder="Thêm món ăn">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ghi chú:</label>
                                <textarea class="form-control message-text-note" placeholder="Viết ghi chú"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" type="button" id="edit-new-menu" class="btn btn-primary">Hoàn tất</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop