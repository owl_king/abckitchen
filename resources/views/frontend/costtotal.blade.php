@extends('frontend')

@section('title')
    Xem thống kê
@stop

@section('content')
    @if($list_month[0] != 0)
        <div class="container-div">
            <div class="row">
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-calculator"></i> THỐNG KÊ CHI PHÍ ĂN UỐNG THÁNG
                        <select class="selecte-month">
                            @foreach($list_month as $month)
                                <option>{{ $month }}</option>
                            @endforeach
                        </select>
                    </li>
                    {{--<li class="active">--}}
                        {{--Tình trạng:--}}
                    {{--</li>--}}
                    <li class="active">
                        @if($top_menu[0]->check_pay == 1)
                            <div class="alert alert-seven alert-success alert-check-pay"><h4>Bạn đã thanh toán <i class="fa fa-check-circle"></i></h4></div>
                        @else
                            <div class="alert alert-seven alert-warning alert-check-pay"><h4>Bạn chưa thanh toán <i class="fa fa-circle-o"></i></h4></div>
                        @endif
                    </li>
                </ol>
                {{--<div class="alert alert-success alert-check-pay"><h4>Bạn đã thanh toán <i class="fa fa-check-circle"></i></h4></div>--}}
            </div>

            <table class="tomorrow-menu-show cost-total-show">
                <thead>
                    <tr>
                        <th class="th-date">Ngày tháng</th>
                        <th>Giờ đăng ký</th>
                        <th>Số tiền</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                @foreach($top_menu as $menu)
                    <tr>
                        <td><span>{{ $menu->date }}</span></td>
                        <td class="time-show"><span>{{ $menu->time }}</span></td>
                        <td class="price">{{ $menu->total_cost}}</td>
                        <td class="details"><a id="{{ $menu->id }}" href="#" data-toggle="modal" data-target="#detailsModal">Xem chi tiết</a></td>
                    </tr>
                @endforeach
                <tr class="total">
                    <td><span>Tổng tiền</span></td>
                    <td></td>
                    <td class="price">{{ $total }}</td>
                    <td></td>
                </tr>
                </tbody>

            </table>

            <!-- Modal -->
            <div class="modal fade show-details" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="detailsModalLabel"><i class="fa fa-cutlery"></i> Xem chi tiết</h4>
                        </div>
                        <div class="modal-body">
                            <table class="tomorrow-menu-show show-details-food" style="box-shadow: none; height: auto;">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    @else
        <div class="container-div">
            <div class="row">
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-calculator"></i> THỐNG KÊ CHI PHÍ ĂN UỐNG
                    </li>
                </ol>
            </div>

            <table class="tomorrow-menu-show cost-total-show">
                <thead>
                <tr>
                    <th class="th-date">Ngày tháng</th>
                    <th>Giờ đăng ký</th>
                    <th>Số tiền</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td colspan="4" style="text-align: center; padding: 50px;">Bạn chưa đăng ký ăn trong tháng nào!</td>
                </tr>

                <tr class="total">
                    <td><span>Tổng tiền</span></td>
                    <td></td>
                    <td class="price">0</td>
                    <td></td>
                </tr>
                </tbody>

            </table>
        </div>
    @endif
@stop