<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    protected $table = 'menus';

    protected $fillable =['date_published', 'note', 'total_cost'];

    public $timestamps = true;

    public function foodmenu()
    {
        return $this->hasMany('App\Foodmenu');
    }

    public function usermenu()
    {
        return $this->hasMany('App\Usermenu');
    }
}
