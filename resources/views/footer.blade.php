<footer>
    <div id="info" class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <p class="product">ABCKITCHEN</p>
                <p class="description">Sản phẩm giúp nhà ăn quản lý và tổ chức một cách nhanh chóng hiệu quả. Hỗ trợ
                người lao động đăng ký ăn trực tuyến nhằm tiết kiệm tối đa thời gian cho người lao động</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 ">
                <ul>
                    <li class="title">Hướng dẫn sử dụng</li>
                    <li><a href="#">Lấy lại mật khẩu</a></li>
                    <li><a href="#">Đăng ký / Hủy bữa ăn</a></li>
                    <li><a href="#">Xem / Thanh toán chi phí</a></li>
                    <li><a href="#">Phản hồi về abckitchen</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <ul>
                    <li class="title">Dinh dưỡng bữa ăn</li>
                    <li><a href="#">Bữa sáng</a></li>
                    <li><a href="#">Bữa trưa</a></li>
                    <li><a href="#">Bữa tối</a></li>
                    <li><a href="#">Bữa khuya</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <ul>
                    <li class="title">Về chúng tôi</li>
                    <li><a href="#">Giới thiệu</a></li>
                    <li><a href="#">Đăng ký</a></li>
                    <li><a href="#">Đăng nhập</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div id="power-by" class="container-fluid">
        <div class="row">
            <div id="Powerby">
                Một sản phẩm của <a href="#" title="" target="_blank"><strong>Kee + King = UET</strong></a><br>
                © Copyright 2015 - 2020 abckitchen – Ăn là sống
            </div>
        </div>
    </div>
</footer>