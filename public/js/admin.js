/*
Script for controlling Ajax Tab
 */
$(document).ready(function()
{
    //khi click vao tab danh sach mon an thi se hien ra list food bang ajax
    $('[data-toggle="tabajax"]').click(function(e)
    {
        e.preventDefault();
        var $this = $(this);
        if($this.attr('role') == 'ajax')
        {
            loadurl = $this.attr('href');
            console.log(loadurl);
            targ = $this.attr('data-target');
            $(targ).html("");
            $.getJSON(loadurl, function(data) {
                var input = "";
                if(data.length == 0){
                    var input = "Hiện tại chưa có món ăn trong cơ sở dữ liệu";
                    $(targ).append($('<p>').html(input));
                }
                else{
                    var rownumber = Math.ceil(data.length/4) ;

                    for (var i = 0 ; i < rownumber ; i++){
                        var row = $("<div class='row cs-style-3'></div>");
                        if(i == rownumber - 1){
                            for (var j = i*4 ; j < data.length ; j++){
                                var name =$('<p>').html(data[j]["name"].toUpperCase());
                                var img = $('<img>').attr('src', data[j]["img"]);
                                var figcaption = $("<figcaption></figcaption>");
                                var h3 = $('<h3>').html(data[j]["name"].toUpperCase());
                                var span = $('<span>').html(data[j]["cost"]);
                                var div_btn = $('<div class="group-btn"></div>')
                                var deleteBtn = $('<button>').html("Xóa");
                                var editBtn = $('<button>').html("Sửa");
                                deleteBtn.attr('class', 'btn btn-danger delete-food');
                                editBtn.attr('class', 'btn btn-primary edit-food');
                                editBtn.attr('data-toggle', 'modal');
                                editBtn.attr('data-target', '#show-food');
                                div_btn.append(editBtn, deleteBtn);
                                figcaption.append(h3, span, div_btn);
                                var afood = $("<div class='col-md-3 col-sm-3 li'></div>");
                                var figure = $("<figure></figure>");
                                figure.attr("id", data[j]["id"]);
                                figure.append(img, figcaption, name);
                                afood.append(figure);
                                row.append(afood);
                            }
                        }
                        else{
                            for (var j = i*4 ; j < i*4+4 ; j++){
                                var name =$('<p>').html(data[j]["name"].toUpperCase());
                                var img = $('<img>').attr('src', data[j]["img"]);
                                var figcaption = $("<figcaption></figcaption>");
                                var h3 = $('<h3>').html(data[j]["name"].toUpperCase());
                                var span = $('<span>').html(data[j]["cost"]);
                                var div_btn = $('<div class="group-btn"></div>')
                                var deleteBtn = $('<button>').html("Xóa");
                                var editBtn = $('<button>').html("Sửa");
                                deleteBtn.attr('class', 'btn btn-danger delete-food');
                                editBtn.attr('class', 'btn btn-primary edit-food');
                                editBtn.attr('data-toggle', 'modal');
                                editBtn.attr('data-target', '#show-food');
                                div_btn.append(editBtn, deleteBtn);
                                figcaption.append(h3, span, div_btn);
                                var afood = $("<div class='col-md-3 col-sm-3 li'></div>");
                                var figure = $("<figure></figure>");
                                figure.attr("id", data[j]["id"]);
                                figure.append(img, figcaption, name);
                                afood.append(figure);
                                row.append(afood);
                            }
                        }
                        $(targ).append(row);
                    }
                }
            });
            $this.tab('show');
            return false;
        }
        else
        {

            $this.tab('show');
            return false;
        }

        e.preventDefault();
    });

    //kiem tra validation form khi nhap du lieu vao form tao mon an moi
    $('.form-food').submit(function(e){

        var formURL = $('.form-food').attr('action');
        var name = $('#name_id').val().toLowerCase();
        var cost = $('#cost_id').val();
        var img = $('#img_id').val();

        $.ajax({
            url:formURL,
            type:'post',
            data: {name:name, cost:cost, img:img, _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                alert('Món ăn đã tồn tại trong cơ sở dữ liệu, nhập tên món ăn khác!');
                //swal("Tên món ăn đã có trong dữ liệu!", "Hãy nhập tên món ăn khác", "warning");
                console.log(errorThrown);
            }
        })
            .done(function (data) {
                if (data.fail == true){
                    //console.log('loi roi');
                    var show_error = $('ul.errors-food');
                    show_error.html("");
                    var li = "";
                    for (var key in data.errors){
                        li += '<li>' + data.errors[key][0] + '</li>';
                    }

                    show_error.append(li);
                    show_error.css("display", "block");
                }else{
                    console.log('ok roi');
                    var show_error = $('ul.errors-food');
                    show_error.html("");
                    show_error.css("display", "none");
                    $('#name_id').val("");
                    $('#cost_id').val("");
                    $('#img_id').val("");
                    alert('Thêm món ăn mới thành công');
                }

            })
            .fail(function (data) {
                console.log('fail');
            });
        e.preventDefault();
    });

});

//khi reload lai trang admin/foods thi hien ngay cai tab danh sach mon an dau tien
$(function(){
    $('ul.tab-show-foods > li').each(function(){

        var $this = $(this);
        if ($this.hasClass('active') && $this.is(':first-child')){

            var $this = $(this).children();
            if($this.attr('role') == 'ajax')
            {
                loadurl = $this.attr('href');
                targ = $this.attr('data-target');
                $(targ).html("");
                $.getJSON(loadurl, function(data) {
                    var input = "";
                    if(data.length == 0){
                        var input = "Hiện tại chưa có món ăn trong cơ sở dữ liệu";
                        $(targ).append($('<p>').html(input));
                    }
                    else{
                        var rownumber = Math.ceil(data.length/4) ;

                        for (var i = 0 ; i < rownumber ; i++){
                            var row = $("<div class='row cs-style-3'></div>");
                            if(i == rownumber - 1){
                                for (var j = i*4 ; j < data.length ; j++){
                                    var name =$('<p>').html(data[j]["name"].toUpperCase());
                                    var img = $('<img>').attr('src', data[j]["img"]);
                                    var figcaption = $("<figcaption></figcaption>");
                                    var h3 = $('<h3>').html(data[j]["name"].toUpperCase());
                                    var span = $('<span>').html(data[j]["cost"]);
                                    var div_btn = $('<div class="group-btn"></div>')
                                    var deleteBtn = $('<button>').html("Xóa");
                                    var editBtn = $('<button>').html("Sửa");
                                    deleteBtn.attr('class', 'btn btn-danger delete-food');
                                    editBtn.attr('class', 'btn btn-primary edit-food');
                                    editBtn.attr('data-toggle', 'modal');
                                    editBtn.attr('data-target', '#show-food');
                                    div_btn.append(editBtn, deleteBtn);
                                    figcaption.append(h3, span, div_btn);
                                    var afood = $("<div class='col-md-3 col-sm-3 li'></div>");
                                    var figure = $("<figure></figure>");
                                    figure.append(img, figcaption, name);
                                    figure.attr("id", data[j]["id"]);
                                    afood.append(figure);
                                    row.append(afood);
                                }
                            }
                            else{
                                for (var j = i*4 ; j < i*4+4 ; j++){
                                    var name =$('<p>').html(data[j]["name"].toUpperCase());
                                    var img = $('<img>').attr('src', data[j]["img"]);
                                    var figcaption = $("<figcaption></figcaption>");
                                    var h3 = $('<h3>').html(data[j]["name"].toUpperCase());
                                    var span = $('<span>').html(data[j]["cost"]);
                                    var div_btn = $('<div class="group-btn"></div>')
                                    var deleteBtn = $('<button>').html("Xóa");
                                    var editBtn = $('<button>').html("Sửa");
                                    deleteBtn.attr('class', 'btn btn-danger delete-food');
                                    editBtn.attr('class', 'btn btn-primary edit-food');
                                    editBtn.attr('data-toggle', 'modal');
                                    editBtn.attr('data-target', '#show-food');
                                    div_btn.append(editBtn, deleteBtn);
                                    figcaption.append(h3, span, div_btn);
                                    var afood = $("<div class='col-md-3 col-sm-3 li'></div>");
                                    var figure = $("<figure></figure>");
                                    figure.attr("id", data[j]["id"]);
                                    figure.append(img, figcaption, name);
                                    afood.append(figure);
                                    row.append(afood);
                                }
                            }
                            $(targ).append(row);
                        }
                    }
                });
                $this.tab('show');
                return false;
            }
            else
            {
                $this.tab('show');
                return false;
            }
        }
    });

});

//update food
$(function () {
    //khi click vao nut edit
    $(document).on("click", ".edit-food", function () {

        var figure = $(this).parent().parent().parent();
        var img = figure.find('img').attr('src');
        var name = figure.find('h3').html().toLowerCase();
        var cost = figure.find('span').html();
        $("#food-name").val(name);
        $("#food-cost").val(cost);
        $("#food-img").val(img);
        var id = figure.attr("id");
        $("#food-id").val(parseInt(id));

    });
    //khi click vao nut update
    $('.form-update-food').submit(function(e){
        var formURL = $('.form-update-food').attr('action');
        var id = $('#food-id').val();
        var name = $('#food-name').val().toLowerCase();
        var cost = $('#food-cost').val();
        var img = $('#food-img').val();

        formURL += '/'+id;

        $.ajax({
            url: formURL,
            type:'post',
            data: {name:name, cost:cost, img:img, _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function(data){
                if (data.success == true){
                    $('#show-food').modal('hide');
                    alert("Cập nhật món ăn "+ data.newFood['name'].toUpperCase() +" thành công");
                    var figure = $("figure[id="+data.newFood['id']+"]");
                    //console.log(figure);
                    var h3 = figure.find('h3');
                    h3.html(data.newFood['name'].toUpperCase());
                    var span = figure.find('span');
                    span.html(data.newFood['cost']);
                    var p = figure.find('p');
                    p.html(data.newFood['name'].toUpperCase());
                    var img = figure.find('img');
                    img.attr('src', data.newFood['img']);
                }
            })
            .error(function (data) {
                console.log('error');
            });

        e.preventDefault();
    });

    //delete mon an
    $(document).on("click", ".delete-food", function (e) {

        var figure = $(this).parent().parent().parent();
        var name = figure.find('h3').html();

        if (confirm("Bạn muốn xóa món ăn "+name+" khỏi dữ liệu?")){
            var id = figure.attr("id");
            var formURL = window.location.href;
            formURL += "/deleteFood/"+id;
            console.log(formURL);
            $.ajax({
                url: formURL,
                type:'post',
                data: {_token:$('meta[name="_token"]').val()},
                dataType: 'json',
                cache: false,
                success : function(data){
                    console.log("ajax sent success");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                    console.log(errorThrown);
                }
            })
                .done(function(data){
                    if (data.success){
                        //load lai trang show food
                        var showAll = $("a[role=ajax]");
                        var formURL = window.location.href;
                        formURL += "/showfood";
                        var targ = showAll.attr('data-target');
                        $(targ).html("");
                        $.getJSON(formURL, function(data) {
                            console.log(data);
                            var input = "";
                            if(data.length == 0){
                                var input = "Hiện tại chưa có món ăn trong cơ sở dữ liệu";
                                $(targ).append($('<p>').html(input));
                            }
                            else{
                                var rownumber = Math.ceil(data.length/4) ;

                                for (var i = 0 ; i < rownumber ; i++){
                                    var row = $("<div class='row cs-style-3'></div>");
                                    if(i == rownumber - 1){
                                        for (var j = i*4 ; j < data.length ; j++){
                                            var name =$('<p>').html(data[j]["name"].toUpperCase());
                                            var img = $('<img>').attr('src', data[j]["img"]);
                                            var figcaption = $("<figcaption></figcaption>");
                                            var h3 = $('<h3>').html(data[j]["name"].toUpperCase());
                                            var span = $('<span>').html(data[j]["cost"]);
                                            var div_btn = $('<div class="group-btn"></div>')
                                            var deleteBtn = $('<button>').html("Xóa");
                                            var editBtn = $('<button>').html("Sửa");
                                            deleteBtn.attr('class', 'btn btn-danger delete-food');
                                            editBtn.attr('class', 'btn btn-primary edit-food');
                                            editBtn.attr('data-toggle', 'modal');
                                            editBtn.attr('data-target', '#show-food');
                                            div_btn.append(editBtn, deleteBtn);
                                            figcaption.append(h3, span, div_btn);
                                            var afood = $("<div class='col-md-3 col-sm-3 li'></div>");
                                            var figure = $("<figure></figure>");
                                            figure.append(img, figcaption, name);
                                            figure.attr("id", data[j]["id"]);
                                            afood.append(figure);
                                            row.append(afood);
                                        }
                                    }
                                    else{
                                        for (var j = i*4 ; j < i*4+4 ; j++){
                                            var name =$('<p>').html(data[j]["name"].toUpperCase());
                                            var img = $('<img>').attr('src', data[j]["img"]);
                                            var figcaption = $("<figcaption></figcaption>");
                                            var h3 = $('<h3>').html(data[j]["name"].toUpperCase());
                                            var span = $('<span>').html(data[j]["cost"]);
                                            var div_btn = $('<div class="group-btn"></div>')
                                            var deleteBtn = $('<button>').html("Xóa");
                                            var editBtn = $('<button>').html("Sửa");
                                            deleteBtn.attr('class', 'btn btn-danger delete-food');
                                            editBtn.attr('class', 'btn btn-primary edit-food');
                                            editBtn.attr('data-toggle', 'modal');
                                            editBtn.attr('data-target', '#show-food');
                                            div_btn.append(editBtn, deleteBtn);
                                            figcaption.append(h3, span, div_btn);
                                            var afood = $("<div class='col-md-3 col-sm-3 li'></div>");
                                            var figure = $("<figure></figure>");
                                            figure.attr("id", data[j]["id"]);
                                            figure.append(img, figcaption, name);
                                            afood.append(figure);
                                            row.append(afood);
                                        }
                                    }
                                    $(targ).append(row);
                                }
                            }
                        });
                        showAll.tab('show');
                    }
                })
                .error(function (data) {
                    console.log('error');
                });
        }else{
            return false;
        }
        e.preventDefault();
    });

});

$(document).ready(function(){
    $("#mytable #checkall").click(function () {
        if ($("#mytable #checkall").is(':checked')) {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });

    $("[data-toggle=tooltip]").tooltip();
});

$(document).ready(function(){
    $("#huydanhdau").click(function () {
        $("#mytable input[type=checkbox]").each(function () {
            $(this).prop("checked", false);
        });
    });
});

//hien menu da chon
function displayResult(item,val,text,cost) {
    $('.show-list-food-menu').find('.alert-danger').remove();
    //console.log($(this));
    if ($('#show-create-menu').css('display') == 'none'){
        var str = $('#show-edit-menu').find('.show-list-food-menu').html();
        var total_cost = $('#show-edit-menu').find('.show-cost-food-menu strong').html();
    }else{
        var str = $('#show-create-menu').find('.show-list-food-menu').html();
        var total_cost = $('#show-create-menu').find('.show-cost-food-menu strong').html();
    }

    //console.log(cost);
    //var count = (str.match(/alert-menu/g) || []).length;
    str += '<div class="alert alert-block alert-success alert-menu">'+'Món '+' :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
        '<input type="hidden" name="id-food-choose" value="'+val+'" cost="'+cost+'" />'+
        '<strong>' + text + '</strong>' +
        '<button type="button" class="del-food-menu">' +
        '<span class="glyphicon glyphicon-remove glyphicon-del-food-menu" aria-hidden="true"></span>' +
        '</button></div>';
    $('.show-list-food-menu').html(str);
    total_cost = parseInt(total_cost) + parseInt(cost);
    $('.show-cost-food-menu strong').html(total_cost);

    $('.food-menu-name').val('');
    $('.food-menu-name').focus();
}

//load danh sach cac mon an trong database
$(function () {
    $(document).on("click", ".create-edit-btn", function (e) {

        $('.show-list-food-menu').html("");
        $('.food-menu-name').val("");
        $('.message-text-note').val("");
        $('.show-cost-food-menu strong').html("0");
        var foods = [];
        if (typeof location.origin === 'undefined')
            location.origin = location.protocol + '//' + location.host;
        var loadurl = location.origin;
        loadurl += '/admin/foods/showfood';
        $.getJSON(loadurl, function (data) {
            if (data.length != 0){
                console.log(data);
                for (var i=0; i<data.length; i++){
                    foods.push({id:data[i]['id'], name: data[i]['name'], cost: data[i]['cost']});
                }
                $('.food-menu-name').typeahead({
                    source: foods,
                    itemSelected: displayResult
                });
            }
        });
    });
});

$(function () {
    $(document).on("click", ".del-food-menu", function (e) {
        var del = $(this).parent();
        var costdel = del.find('input').attr('cost');

        if ($('#show-create-menu').css('display') == 'none'){
            var total_cost = $('#show-edit-menu .show-cost-food-menu strong').html();
        }else{
            var total_cost = $('#show-create-menu .show-cost-food-menu strong').html();
        }

        total_cost = parseInt(total_cost) - parseInt(costdel);
        $('.show-cost-food-menu strong').html(total_cost);
        del.remove();
    });
});


$(function () {
    $('.form-menu').submit(function(e){
        var p = $(this).parent().parent().parent();
        var list_choose = p.find('.show-list-food-menu').children();
        //console.log(list_choose[0]);
        var list_id = [];
        if(list_choose.length == 0 || list_choose.length == 1 && ($(list_choose[0]).hasClass("alert-danger"))){
            console.log('phai nhap ten thuc an moi duoc');
        }else {
            for (var i = 0; i < list_choose.length; i++) {
                var val = list_choose[i].childNodes[1].getAttribute('value');
                list_id.push(val);
            }
            console.log(list_id);
        }
        var date = $('#calendar-output').html();
        var note = $('.message-text-note').val();
        var cost = $('.show-cost-food-menu strong').html();

        //console.log(note);
        var formURL = $('.form-menu').attr('action');

        $.ajax({
            url: formURL,
            type:'post',
            data: {id:list_id, date:date, note:note, cost:cost, _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function(data){
                if (data.success == false) {
                    var str = $('.show-list-food-menu').html();
                    var li = "<ul>";
                    for (var key in data.errors){
                        li += '<li class="li-errors">' + data.errors[key][0] + '</li>';
                    }
                    li += '</ul>';
                    str = '<div class="alert alert-block alert-danger alert-menu">'+li+'</div>';
                    $('.show-list-food-menu').html(str);

                }else{
                    $('#show-create-menu').modal('hide');
                    alert('Đã tạo thực đơn mới thành công!');
                    $('#create-menu').addClass('disabled');
                    $('#edit-menu').removeClass('disabled');
                    $('#delete-menu').removeClass('disabled');
                    //location.reload();
                    //hien list thuc an trong menu vua tao
                    $('.list-menu-show').html("");
                    var str = "";
                    for (var i=0;i<data.list_food.length;i++){
                        var li = '<li id="'+data.list_food[i]['id']+'" cost="'+data.list_food[i]['cost']+'">'+data.list_food[i]['name'].capitalize()+'</li>';
                        str += li;
                    }
                    $('.list-menu-show').html(str);
                    //hien ghi chu cua menu vua tao
                    if (note != "") {
                        $('.note-menu').html('<p class="p-list-food">' + note + '</p>');
                    }else{
                        $('.note-menu').html('<p class="p-list-food">Không có ghi chú</p>');
                    }

                    $('.tongtien').html(data.cost);

                    //danh dau tich ngay vua tao menu
                    $('.calendar-admin').find('#current-menu').addClass('selected');
                    $('.calendar-admin').find('#current-menu').removeAttr('id');
                    $('.calendar-admin').find('.selected-choose').attr('id','current-menu');

                    var height_container = $('#container-show .table').height();
                    var height_calendar = $('.calendar').height();
                    //console.log(height_container);
                    if (height_calendar<height_container){
                        $('.calendar').height(height_container);
                    }else{
                        $('.calendar').height(375);
                    }
                }
            })
            .error(function (data) {
                console.log('error');
            });

        e.preventDefault();

    });
});

//xoa thuc don
$(function () {
    $(document).on("click", ".delete-menu-btn", function (e) {

        if (confirm("Bạn muốn xóa thực đơn này khỏi dữ liệu?")){
            var date = $('#calendar-output').html();
            //console.log(date);
            var formURL = window.location.href;
            formURL = formURL + '/deletemenu/' + date;
            //console.log(formURL);

            $.ajax({
                url: formURL,
                type:'post',
                data: { _token:$('meta[name="_token"]').val()},
                dataType: 'json',
                cache: false,
                success : function(data){
                    console.log("ajax sent success");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                    console.log(errorThrown);
                }
            })
                .done(function(data){
                    if(data.success ){
                        console.log('xoa menu thanh cong');
                        $('.calendar-admin').find('.selected-choose').removeAttr('id');
                        $('.calendar-admin').find('.selected-choose').removeClass('selected');

                        $('.list-menu-show').html("");
                        $('.list-menu-show').append('<p class="p-list-food">Chưa lên thực đơn cho ngày này</p>');
                        $('.note-menu').html("");
                        $('.note-menu').append('<p class="p-list-food">Không có ghi chú</p>');
                        $('.tongtien').html("0");

                        $('#edit-menu').addClass('disabled');
                        $('#delete-menu').addClass('disabled');
                        $('#create-menu').removeClass('disabled');

                    }
                })
                .error(function (data) {
                    console.log('error');
                });

        }
    });
});

//update thuc don
$(function () {
    $(document).on("click", "#edit-menu", function (e) {
        //console.log('edit');
        $('.show-list-food-menu').html('');
        var str_show_list_food = $('.show-list-food-menu').html();
        $('.food-menu-name').val("");
        var note = $('.p-list-food').html();
        if (note != 'Không có ghi chú') $('.message-text-note').val(note);
        else $('.message-text-note').val('');
        $('.show-cost-food-menu strong').html($('.tongtien').html());

        var ul_list = $('.list-menu-show').children();
        for (var i=0; i<ul_list.length; i++){
            //console.log($(ul_list[i]).attr('id'));
            str_show_list_food += '<div class="alert alert-block alert-success alert-menu">'+'Món '+' :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<input type="hidden" name="id-food-choose" value="'+$(ul_list[i]).attr('id')+'" cost="'+$(ul_list[i]).attr('cost')+'" />'+
            '<strong>' + $(ul_list[i]).html() + '</strong>' +
            '<button type="button" class="del-food-menu">' +
            '<span class="glyphicon glyphicon-remove glyphicon-del-food-menu" aria-hidden="true"></span>' +
            '</button></div>';
        }
        $('.show-list-food-menu').html(str_show_list_food);
    });
});

$(function () {
    $('.form-menu-edit').submit(function(e){
        var p = $(this).parent().parent().parent();
        var list_choose = p.find('.show-list-food-menu').children();
        //console.log(list_choose[0]);
        var list_id = [];
        if(list_choose.length == 0 || list_choose.length == 1 && ($(list_choose[0]).hasClass("alert-danger"))){
            console.log('phai nhap ten thuc an moi duoc');
        }else {
            for (var i = 0; i < list_choose.length; i++) {
                var val = list_choose[i].childNodes[1].getAttribute('value');
                list_id.push(val);
            }
            console.log(list_id);
        }
        var date = $('#calendar-output').html();
        var note = $($('.form-menu-edit').find('.message-text-note')).val();
        var cost = $($('#show-edit-menu').find('.show-cost-food-menu strong')).html();

        var formURL = $('.form-menu-edit').attr('action');

        $.ajax({
            url: formURL,
            type:'post',
            data: {id:list_id, date:date, note:note, cost:cost, _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function(data){
                console.log('done');
                if (data.success == false) {
                    console.log('trong du lieu');
                    var str = $($('#show-edit-menu').find('.show-list-food-menu')).html();
                    var li = "<ul>";
                    for (var key in data.errors){
                        li += '<li class="li-errors">' + data.errors[key][0] + '</li>';
                    }
                    li += '</ul>';
                    str = '<div class="alert alert-block alert-danger alert-menu">'+li+'</div>';
                    $($('#show-edit-menu').find('.show-list-food-menu')).html(str);
                }else{
                    $('#show-edit-menu').modal('hide');
                    alert('Đã hoàn thành chỉnh sửa thực đơn!');
                    //location.reload();
                    //hien list thuc an trong menu vua tao
                    $('.list-menu-show').html("");
                    var str = "";
                    for (var i=0;i<data.list_food.length;i++){
                        var li = '<li id="'+data.list_food[i]['id']+'">'+data.list_food[i]['name'].capitalize()+'</li>';
                        str += li;
                    }
                    $('.list-menu-show').html(str);
                    //hien ghi chu cua menu vua tao
                    if (note != "") {
                        $('.note-menu').html('<p class="p-list-food">' + note + '</p>');
                    }else{
                        $('.note-menu').html('<p class="p-list-food">Không có ghi chú</p>');
                    }
                    $('.tongtien').html($('#show-edit-menu .show-cost-food-menu strong').html());

                    var height_container = $('#container-show .table').height();
                    var height_calendar = $('.calendar').height();
                    //console.log(height_container);
                    if (height_calendar<height_container){
                        $('.calendar').height(height_container);
                    }else{
                        $('.calendar').height(375);
                    }
                }
            })
            .error(function (data) {
                console.log('error');
            });

        e.preventDefault();

    });
});


$(function () {
    $(document).on('click', ".detail-admin", function (event) {
        event.preventDefault();
        //console.log($(this));
        $('.check-state-admin').html('Chưa thanh toán');
        $('.total-cost-admin').html('0');
        $('.select-month-admin').removeAttr('id');
        $('.select-month-admin').attr('id', $(this).attr('id'));

        var user_id = $(this).attr('id');
        if (typeof location.origin === 'undefined')
            location.origin = location.protocol + '//' + location.host;
        var formURL = location.origin;
        formURL += '/admin/users/showMembers/showpay/' + user_id;
        //console.log(formURL);
        //
        $.ajax({
            url: formURL,
            type:'post',
            data: { _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function(data){
                if(data.success){
                    //console.log(data.topmenu);

                    $('.select-month-admin').html('');
                    var str_month = '';
                    for (var i=0; i<data.listmonth.length; i++){
                        str_month += '<option>'+data.listmonth[i] + '</option>';
                    }
                    $('.select-month-admin').html(str_month);
                    $('.total-cost-admin').html(data.total);

                    if (data.topmenu[0]['check_pay']){
                        $('.check-state-admin').html('Đã thanh toán');
                        $('.check-state-admin').removeAttr('id');
                        $('.check-state-admin').attr('id', '1');
                    }else{
                        $('.check-state-admin').html('Chưa thanh toán');
                        $('.check-state-admin').removeAttr('id');
                        $('.check-state-admin').attr('id', '0');
                    }

                    if ($('.check-state-admin').attr('id') == '1'){
                        $('.set-thanhtoan').addClass('disabled');
                        $('.huy-thanhtoan').removeClass('disabled');
                    }else{
                        $('.set-thanhtoan').removeClass('disabled');
                        $('.huy-thanhtoan').addClass('disabled');
                    }

                    $('.submit-state-admin').removeClass('disabled');

                }else{
                    $('.select-month-admin').html('');
                    $('.submit-state-admin').addClass('disabled');
                    $('.set-thanhtoan').addClass('disabled');
                    $('.huy-thanhtoan').addClass('disabled');
                }
            })
            .error(function (data) {
                console.log('error');
            });



    });

    $('.select-month-admin').change(function(){
        var input_month = $(this).val();
        //console.log(input_month);
        if (typeof location.origin === 'undefined')
            location.origin = location.protocol + '//' + location.host;
        var formURL = location.origin;
        formURL += '/admin/users/showMembers/showpay/'+ $(this).attr('id')+ '/showmonthadmin/'+input_month;
        console.log(formURL);

        $.ajax({
            url: formURL,
            type:'post',
            data: { _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function(data){
                if(data.success){
                    //console.log(data.topmenu);
                    $('.total-cost-admin').html(data.total);

                    if (data.topmenu[0]['check_pay']){
                        $('.check-state-admin').html('Đã thanh toán');
                        $('.check-state-admin').removeAttr('id');
                        $('.check-state-admin').attr('id', '1');
                    }else{
                        $('.check-state-admin').html('Chưa thanh toán');
                        $('.check-state-admin').removeAttr('id');
                        $('.check-state-admin').attr('id', '0');
                    }

                    if ($('.check-state-admin').attr('id') == '1'){
                        $('.set-thanhtoan').addClass('disabled');
                        $('.huy-thanhtoan').removeClass('disabled');
                    }else{
                        $('.set-thanhtoan').removeClass('disabled');
                        $('.huy-thanhtoan').addClass('disabled');
                    }
                }
            })
            .error(function (data) {
                console.log('error');
            });
    });


    $(document).on('click', ".set-thanhtoan", function (event) {
        $('.check-state-admin').html('Đã thanh toán');
        $('.check-state-admin').removeAttr('id');
        $('.check-state-admin').attr('id', '1');

        $('.set-thanhtoan').addClass('disabled');
        $('.huy-thanhtoan').removeClass('disabled');
    });
    $(document).on('click', ".huy-thanhtoan", function (event) {
        $('.check-state-admin').html('Chưa thanh toán');
        $('.check-state-admin').removeAttr('id');
        $('.check-state-admin').attr('id', '0');

        $('.set-thanhtoan').removeClass('disabled');
        $('.huy-thanhtoan').addClass('disabled');
    });

    $(document).on('click', ".submit-state-admin", function (event) {
        var user_id = $($(this).parent().parent()).find('select').attr('id');
        //console.log(user_id);
        var state = $('.check-state-admin').attr('id');
        var month = $('.select-month-admin').val();

        if (typeof location.origin === 'undefined')
            location.origin = location.protocol + '//' + location.host;
        var formURL = location.origin;
        formURL += '/admin/users/showMembers/showpay/'+ user_id + '/submitstate/'+month;
        //console.log(formURL);

        $.ajax({
            url: formURL,
            type:'post',
            data: {state:state, _token:$('meta[name="_token"]').val()},
            dataType: 'json',
            cache: false,
            success : function(data){
                console.log("ajax sent success");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // alert('HTTP Error: '+errorThrown+' | Error Message: '+textStatus);
                console.log(errorThrown);
            }
        })
            .done(function(data){
                if(data.success){
                    //console.log(data.state);
                    $('#pay-modal').modal('hide');
                    alert('Xác nhận thành công!');
                }
            })
            .error(function (data) {
                console.log('error');
            });
    });
});


$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });

});