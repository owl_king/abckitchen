<table class="tomorrow-menu-show col-md-8">
    <tbody>
        @if(isset($food_menu) && !empty($food_menu))
            <?php $total = 0;?>
            @foreach($food_menu as $food)
                <?php $total += $food->food->cost; ?>
                <tr>
                    <td><span>{{ $food->food->name }}</span></td>
                    <td class="price">{{ $food->food->cost }}</td>
                </tr>
            @endforeach
            <tr class="total">
                <td><span>Thành tiền</span></td>
                <td class="price">{{ $total }}</td>
            </tr>
        @else
            <tr>
                <td colspan="2"><span>Chưa có thực đơn cho ngày mai</span></td>
            </tr>
        @endif
    </tbody>

</table>

<label class="col-md-4 tinhtrang"><hr>

    @if(isset($food_menu) && !empty($food_menu))
        <label class="label-tinhtrang">
            <h3 class="h3-tinhtrang">Trạng thái</h3>
            <div class="order-tomorrow-state">
            {{--<div class="alert alert-warning" role="alert"><h4>Bạn chưa đăng ký</h4></div>--}}
            </div>
        </label>
        <div class="ch-grid-btn">
            <div class="li-btn li-tomorrow-menu">
                {{--<a id="submit-tomorrow-menu">--}}
                    {{--<div class="ch-item">--}}
                        {{--<div class="ch-info">--}}
                            {{--<div class="ch-info-front ch-img-1">--}}
                                {{--<h3>Đăng ký ngay</h3>--}}
                            {{--</div>--}}
                            {{--<div class="ch-info-back">--}}
                                {{--<i class="fa fa-plus-square"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            </div>
        </div>
    @endif
</label>

