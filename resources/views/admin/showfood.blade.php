@extends('...backend')

@section('title')
    Quản lý món ăn
@stop

@section('content')
    <div style="min-height: 342px;" id="page-wrapper">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-cutlery"></i> QUẢN LÝ MÓN ĂN
                </li>
            </ol>
        </div>
    <div role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs tab-show-foods" role="tablist">
            <li class="active">
                <a data-target="#list-food"  href="{{ url('/admin/foods/showfood') }}" role="ajax" aria-controls="list-food" data-toggle="tabajax">Danh sách món ăn</a>
            </li>
            <li>
                <a data-target="#create-food" aria-controls="create-food" data-toggle="tabajax">Thêm món ăn</a>
            </li>

        </ul>
        <form class="search-form sss" action="">
            <div class="form-group fff has-feedback">
                <label for="search" class="sr-only">Search</label>
                <input class="form-control" name="search" id="search" placeholder="Tìm kiếm món ăn" type="text">
                <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
        </form>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active fade in" id="list-food">

            </div>
            <div role="tabpanel" class="tab-pane fade in" id="create-food">
                <form class="form-horizontal form-food" action="{{ url('/admin/foods/createfood') }}" method="POST">
{{--                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">--}}

                        <ul class="alert alert-danger errors-food">
                        </ul>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên món ăn</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="name_id" name="name" placeholder="Nhập tên món ăn" autofocus/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Đơn giá VNĐ</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="cost_id" name="cost" placeholder="Nhập giá thành">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Hình ảnh</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="img_id" name="img" placeholder="Copy đường dẫn ảnh">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Tạo món ăn</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>

    <div class="modal fade" id="show-food" tabindex="-1" role="dialog" aria-labelledby="show-menu" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="show-menu">Sửa món ăn</h4>
                </div>
                <div class="modal-body">
                    {{--{!! Form::model($articles,[ 'method' => 'PATCH', 'action' => ['ArticlesController@update', $articles->id] ]) !!}--}}
                    <form class="form-update-food" action="{{ url('/admin/foods/editFood') }}"  method="POST">
                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="food-id" name="food-id">
                        <ul class="alert alert-danger errors-food">
                        </ul>

                        <div class="form-group">
                            <label for="food-name" class="control-label">Tên món ăn:</label>
                                <input type="text" class="form-control" name="name" id="food-name">
                        </div>
                        <div class="form-group">
                            <label for="food-cost" class="control-label">Đơn giá VNĐ:</label>
                                <input type="text" class="form-control" name="cost" id="food-cost">
                        </div>
                        <div class="form-group">
                            <label for="food-img" class="control-label">Hình ảnh:</label>
                                <input type="text" class="form-control" name="img" id="food-img">
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    {{--<div class="modal fade" id="delete-food" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true" data-backdrop="static" data-keyboard="false">--}}
        {{--<div class="modal-dialog">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>--}}
                    {{--<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}

                    {{--<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>--}}

                {{--</div>--}}
                {{--<div class="modal-footer ">--}}
                    {{--<button type="button" class="btn btn-success yes-delete-food" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>--}}
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- /.modal-content -->--}}
        {{--</div>--}}
        {{--<!-- /.modal-dialog -->--}}
    {{--</div>--}}
    </div>
@stop