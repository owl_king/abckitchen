<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Foodmenu extends Model {

    protected $table = 'food_menu';

    protected $fillable =['food_id', 'menu_id'];

    public $timestamps = false;

    public function food()
    {
        return $this->belongsTo('App\Food');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

}
