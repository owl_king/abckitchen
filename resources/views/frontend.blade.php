<!DOCTYPE html">
<html>
<head>
    <title>@yield('title')</title>

    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>

    <link type="image/x-icon" rel="shortcut icon" href="http://static.bizwebmedia.net/favicon.ico">
    <link href="{{ asset('/css/metro-bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/slick-theme.css') }}">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/metro-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/catslider.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style-front.css') }}" rel="stylesheet">

    <link href="{{ asset('/css/frontend.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <link href="{{ asset('/css/style-front_ie.css') }}" rel="stylesheet">
    <![endif]-->


    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.widget.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/metro.min.js') }}"></script>
    {{--<script src="{{ asset('/js/metro-global.js') }}"></script>--}}
    {{--<script src="{{ asset('/js/metro-locale.js') }}"></script>--}}
    {{--<script src="{{ asset('/js/metro-calendar.js') }}"></script>--}}
    <script src="{{ asset('/js/new.js') }}"></script>
    <script src="{{ asset('/js/jmpress.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.jmslideshow.js') }}"></script>
    <script src="{{ asset('/js/modernizr.custom.48780.js') }}"></script>
    <script src="{{ asset('/js/slick.js') }}"></script>
    <script src="{{ asset('/js/googlemap.js') }}"></script>
    <script src="{{ asset('/js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/js/modernizr.custom.63321.js') }}"></script>
    <script src="{{ asset('/js/jquery.catslider.js') }}"></script>
    <script src="{{ asset('/js/frontend.js') }}"></script>


	<style type="text/css">#Header{background-image: url( {{ asset('images/logo.png') }} ); height:103px; background-repeat: no-repeat;}</style>
</head>
<body>
    <div class="bg_topmenu">
        <nav class="TopMenu">
            <ul class="clearfix">
                <li style="color: #FFFFFF">Xin chào:</li>
                <li class="dropdown">
                    <a class="username" href="#">{{ $username }}<span class="arrow"></span></a>
                    <ul class="sub-menu">
                        <li><a href="#">Thông tin cá nhân</a></li>
                        <li><a href="#">Món ăn ưa thích</a></li>
                        <li><a href="#">Món ăn dị ứng</a></li>
                        <li><a href="#">Cài đặt</a></li>
                    </ul>
                </li>
                <li class="topMenu_Cost">
                    <a href="#">
                        Thông báo
                    </a>
                </li>

                @if($role == 1)
                    <li class="topMenu_Adminpage">
                    <a href="{{ url('/admin') }}">
                        Trang quản trị
                    </a>
                </li>
                @endif

                <li class="topMenu_Logout">
                    <a href="{{ url('/auth/logout') }}">
                        Thoát
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="Clear"></div>
    <div id="bg_header">
        <div id="Header">
            <div id="Logo">
                <div id="LogoContainer">
                    <h1>
                        <a href="{{ url('/home') }}"><span>ABCKITCHEN</span></a>
                    </h1>
                </div>
            </div>
            <div id="Menu">
                <div class="menu-container">
                    <ul id="nav">
                        <li id="nav-1" class="level0 nav-1 first parent actived-menu">
                            <a href="{{ url('/home') }}">
                                <span>Trang chủ</span>
                            </a>
                        </li>
                        <li id="nav-2" class="level0 nav-2 parent">
                            <a href="{{ url('/ordermenu') }}">
                                <span>Đặt món</span>
                            </a>
                        </li>

                        {{--<li id="nav-3" class="level0 nav-3 parent">--}}
                            {{--<a href="#">--}}
                                {{--<span>Thực đơn</span>--}}
                            {{--</a>--}}
                            {{--<ul class="level0">--}}
                                {{--<li id="nav-3-1" class="level1 nav-3-1 first">--}}
                                    {{--<a href="#">--}}
                                        {{--<span>Ngày hôm nay</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                {{--<li id="nav-3-2" class="level1 nav-3-2" >--}}
                                    {{--<a href="#">--}}
                                        {{--<span>Ngày mai</span>--}}
                                    {{--</a>--}}

                                {{--</li>--}}

                                {{--<li id="nav-3-3" class="level1 nav-3-3" >--}}
                                    {{--<a href="#">--}}
                                        {{--<span>Ngày kia</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                            {{--</ul>--}}
                        {{--</li>--}}

                        <li id="nav-4" class="level0 nav-4 parent">
                            <a href="{{ url('/costtotal') }}">
                                <span>Chi phí ăn</span>
                            </a>

                        </li>

                        <li id="nav-5" class="level0 nav-5 parent">
                            <a href="{{ url('/guide') }}">
                                <span>Hướng dẫn</span>
                            </a>

                        </li>

                        <li id="nav-6" class="level0 nav-6 parent">
                            <a href="{{ url('/contact') }}">
                                <span>Liên hệ</span>
                            </a>

                        </li>

                    </ul>
                </div>

                <br class="Clear">
            </div>
            <div class="Clear"></div>
        </div>
    </div>
            <div class="container-front">
                @yield('content')
            </div>

        @include('footer')
    </body>
</html>
