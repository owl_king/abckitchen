<?php namespace App\Http\Controllers;

use App\Food;
use App\Foodmenu;
use App\Http\Controllers\Controller;

use App\Http\Requests\FoodRequest;
use App\Menu;
use App\User;
use App\Usermenu;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Request;
use Response;
use Illuminate\Support\Facades\Auth;

class BackendController extends Controller {

    public function home()
    {
        $username = \Auth::user()->name;

        $time_tmr = Carbon::now()->tomorrow()->toDateString();
        $time_today = Carbon::now()->toDateString();

        $get_menu_tmr = Menu::where('date_published', '=', $time_tmr)->first();

        $count_khauphanngaymai = 0;
        $count_khauphanhomnay = 0;

        if(isset($get_menu_tmr) && $get_menu_tmr->count() != 0) {
            $khauphanngaymai = Usermenu::where('menu_id', '=', $get_menu_tmr->id)->get();
            $count_khauphanngaymai = count($khauphanngaymai);
        }

        $get_menu_today = Menu::where('date_published', '=', $time_today)->first();
        if(isset($get_menu_today) && $get_menu_today->count() != 0) {
            $khauphanhomnay = Usermenu::where('menu_id', '=', $get_menu_today->id)->get();
            $count_khauphanhomnay = count($khauphanhomnay);
        }

        return view('admin.home', compact('username', 'count_khauphanngaymai', 'count_khauphanhomnay'));
    }

    public function menus()
    {
        $username = \Auth::user()->name;
        return view('admin.showmenu', compact('username'));
    }

    public function showCalendar()
    {
        $created_day = DB::table('menus')->select('date_published')->get();
        return Response::json($created_day);
    }

    public function foods()
    {
        $username = \Auth::user()->name;
        return view('admin.showfood', compact('username'));
    }

    public function showFood(){
        $food = Food::orderBy('id', 'desc')->get();
        $foods = $food->toArray();
        return Response::json($foods);
    }

    /**
     * @return mixed
     */
    public function createFood(){
        if (Request::ajax()) {
            $name = Input::get('name');
            $cost = Input::get('cost');
            $img = Input::get('img');

            $inputData = [
                'name' => $name,
                'cost' => $cost,
                'img' => $img,
            ];

            $rules = array(
                'name' => 'required',
                'cost' => 'required',
                'img' => 'required',
            );

            $validation = Validator::make($inputData, $rules);

            if ($validation->fails()) {
//                // if get errors
                return Response::json(array(
                    'fail' => true,
                    'errors' => $validation->getMessageBag()->toArray(),
                ));
            }else{
                // if not get errors
                Food::create(['name' => $name , 'cost' => $cost , 'img' => $img]);

//                $username = \Auth::user()->name;
//                return view('admin.showfood', compact('username'));
                return Response::json(array(
                    'fail' => false,
                ));
//
            }
        }

    }

    public function editFood($id){
        if (Request::ajax()) {
            $name = Input::get('name');
            $cost = Input::get('cost');
            $img = Input::get('img');

            $newFood = ['name' => $name, 'cost' => $cost, 'img' => $img];

            DB::table('foods')->where('id', $id)->update($newFood);

//        $username = \Auth::user()->name;
//        return view('admin.showfood', compact('username'));

            $newShow = ['id' => $id, 'name' => $name, 'cost' => $cost, 'img' => $img];

            return Response::json([
                'success' => true,
                'newFood' => $newShow,
            ]);
        }
    }

    public function deleteFood($id){
        DB::table('foods')->where('id', $id)->delete();
        return Response::json([
            'success' => true,
        ]);
    }

    public function showManagers(){
        $username = \Auth::user()->name;

        if (isset($_GET['sort']) && isset($_GET['title']) && !empty($_GET['sort']) && !empty($_GET['title'])){
            $sort = $_GET['sort'];
            $title = $_GET['title'];
            $data['sort'] = $sort;
            $data['title'] = $title;
            $allAdm = User::where('role', 1)->orderBy($title, $sort)->paginate(2);
//            var_dump($sort, $title);
        }else{
            $allAdm = User::where('role', 1)->paginate(2);
            $data['sort'] = "";
            $data['title'] = "";
            $data = "";
        }

        return view('admin.showManagers', compact('username', 'allAdm', 'data'));
    }

    public function showMembers(){

        $username = \Auth::user()->name;

        if (isset($_GET['sort']) && isset($_GET['title']) && !empty($_GET['sort']) && !empty($_GET['title'])){
            $sort = $_GET['sort'];
            $title = $_GET['title'];
            $data['sort'] = $sort;
            $data['title'] = $title;
            $allMem = User::where('role', 0)->orderBy($title, $sort)->paginate(2);
//            var_dump($sort, $title);
        }else{
            $allMem = User::where('role', 0)->paginate(2);
            $data['sort'] = "";
            $data['title'] = "";
            $data = "";
        }

        return view('admin.showMembers', compact('username', 'allMem', 'data'));
    }

    public function showMenu($id){
        $foods = DB::table('foods')
            ->join('food_menu', 'foods.id', '=', 'food_menu.food_id')
            ->join('menus', 'food_menu.menu_id', '=', 'menus.id')
            ->where('menus.date_published', '=', $id)
            ->get();

        $count_khauphanhomnay = 0;
        $get_menu_today = Menu::where('date_published', '=', $id)->first();
        if (isset($get_menu_today) && count($get_menu_today)){
            $khauphanhomnay = Usermenu::where('menu_id', '=', $get_menu_today->id)->get();
            $count_khauphanhomnay = count($khauphanhomnay);
        }

        return Response::json([
            'success' => true,
            'listIdFood' => $foods,
            'count_khauphanhomnay' => $count_khauphanhomnay,
        ]);
    }

    public function createMenu(){
        if (Request::ajax()) {
            $ids = Input::get('id');
            $date = Input::get('date');
            $note = Input::get('note');
            $cost = Input::get('cost');

            $inputData = [
                'menu' => $ids,
            ];
            $rules = array(
                'menu' => 'required'
            );

            $validation = Validator::make($inputData, $rules);
            if ($validation->fails()) {
                return Response::json(array(
                    'success' => false,
                    'errors' => $validation->getMessageBag()->toArray(),
                ));
            }else{
                $menu = new Menu([
                    'date_published' => $date,
                    'note' => $note,
                    'total_cost' => $cost,
                ]);
                $menu->save();

                $food_list = [];
                foreach($ids as $id){
                    $food_menu = new Foodmenu();
                    $food = Food::find($id);
                    $food_menu->food_id = $food->id;
                    $food_menu->menu_id = $menu->id;
                    $food_menu->save();
                    array_push($food_list, $food);
                }

                return Response::json(array(
                    'success' => true,
                    'list_food' => $food_list,
                    'cost' => $cost
                ));
            }
        }
    }

    public function editMenu(){
        if (Request::ajax()) {
            $ids = Input::get('id');
            $date = Input::get('date');
            $note = Input::get('note');
            $cost = Input::get('cost');

            $inputData = [
                'menu' => $ids,
            ];
            $rules = array(
                'menu' => 'required'
            );

            $validation = Validator::make($inputData, $rules);
            if ($validation->fails()) {
                return Response::json(array(
                    'success' => false,
                    'errors' => $validation->getMessageBag()->toArray(),
                ));
            }else{
                $menu = Menu::first()->where('date_published', '=', $date)->first();
                $menu->update(['note' => $note, 'total_cost' => $cost]);
                $food_menu = Foodmenu::where('menu_id', '=', $menu->id)->get();
//                dd($food_menu);
                foreach($food_menu as $foodmenu){
                    $foodmenu->delete();
                }

                $food_list = [];
                foreach($ids as $id){
                    $food_menu = new Foodmenu();
                    $food = Food::find($id);
                    $food_menu->food_id = $food->id;
                    $food_menu->menu_id = $menu->id;
                    $food_menu->save();
                    array_push($food_list, $food);
                }

                return Response::json(array(
                    'success' => true,
                    'list_food' => $food_list,
                ));
            }
        }
    }

    public function deleteMenu($id){
        DB::table('menus')->where('date_published', $id)->delete();

        return Response::json([
            'success' => true,
        ]);
    }

    public function showpay($id){
        if (Request::ajax()) {
            $months = Usermenu::select(DB::raw('month(created_at) as month'))->where('user_id', '=', $id)->groupby('month')->orderby('month','desc')->get();
            if (count($months) && !empty($months)) {
                $list_month = [];
                foreach ($months as $month) {
                    array_push($list_month, $month->month);
                }
                $lastest_month = $list_month[0];
                $top_menu = DB::table('user_menu')
                    ->select(DB::raw('date(user_menu.created_at) as date'), DB::raw('time(user_menu.created_at) as time'), 'menus.total_cost', 'menus.id', 'user_menu.check_pay')
                    ->join('menus', 'user_menu.menu_id', '=', 'menus.id')
                    ->where('user_id', '=', $id)
                    ->where(DB::raw('month(user_menu.created_at)'), '=', $lastest_month)
                    ->get();
                $total = 0;
                foreach ($top_menu as $menu) {
                    $total += $menu->total_cost;
                }

                return Response::json([
                    'success' => true,
                    'topmenu' => $top_menu,
                    'total' => $total,
                    'listmonth' => $list_month,
                    'user_id' => $id,
                ]);
            }else{
                return Response::json([
                    'success' => false,
                ]);
            }
        }
    }

    public function showMonthAdmin($id, $month){
        if (Request::ajax()) {

            $top_menu = DB::table('user_menu')
                ->select(DB::raw('date(user_menu.created_at) as date'),DB::raw('time(user_menu.created_at) as time'), 'menus.total_cost', 'menus.id', 'user_menu.check_pay')
                ->join('menus', 'user_menu.menu_id', '=', 'menus.id')
                ->where('user_id', '=', $id)
                ->where(DB::raw('month(user_menu.created_at)'), '=' , $month)
                ->get();

            $total = 0;
            foreach($top_menu as $menu){
                $total += $menu->total_cost;
            }

            return Response::json(array(
                'success' => true,
                'topmenu' => $top_menu,
                'total' => $total,
            ));
        }
    }

    public function submitState($id, $month){
        if (Request::ajax()) {
            $state = Input::get('state');
            Usermenu::where('user_id', '=', $id)->where(DB::raw('month(created_at)'), '=', $month)->update(['check_pay' => $state]);

            return Response::json(array(
                'success' => true,
            ));
        }
    }

    public function searchMem(){
        $username = \Auth::user()->name;

        $input = $_GET['search'];

        $allMem = User::where('role', 0)->where('email', 'LIKE', '%'.$input.'%')->orWhere('name', 'LIKE', '%'.$input.'%')->where('role', 0)->paginate(2);

        return view('admin.showMembers', compact('username', 'allMem'));
    }
}
