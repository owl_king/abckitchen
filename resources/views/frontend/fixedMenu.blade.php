<div class="fixed-menu">
    <h3><i class="fa fa-globe"></i> Bạn có thể chọn một hoặc nhiều ngày cố định trong tuần</h3>
    {{--<hr style="width: 30%; margin: auto">--}}
    <ul>
        <li style="margin-left: 15px;">
        {{--<li>--}}
            <input type="checkbox" name="one" id="one" />
            <label for="one">Chủ nhật</label>
        </li>
        <li>
            <input type="checkbox" name="two" id="two" />
            <label for="two">Thứ 2</label>
        </li>
        <li>
            <input type="checkbox" name="three" id="three" />
            <label for="three">Thứ 3</label>
        </li>
        <li>
            <input type="checkbox" name="four" id="four" />
            <label for="four">Thứ 4</label>
        </li>
        <li>
            <input type="checkbox" name="five" id="five" />
            <label for="five">Thứ 5</label>
        </li>
        <li>
            <input type="checkbox" name="six" id="six" />
            <label for="six">Thứ 6</label>
        </li>
        <li style="margin-right: 0px">
            <input type="checkbox" name="seven" id="seven" />
            <label for="seven">Thứ 7</label>
        </li>
    </ul>
    {{--<div>Xác nhận</div>--}}
    <div class="ch-grid-btn fixed-ch-grid-btn">
        <div class="fixed-li-btn li-btn li-fixed-menu">
            <a id="submit-fixed-menu" class="res-fixed-menu">
                <div class="ch-item">
                    <div class="ch-info">
                        <div class="ch-info-front ch-img-1">
                            <h3>Xác nhận</h3>
                        </div>
                        <div class="ch-info-back ch-img-1">
                            <i class="fa fa-plus-square"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>