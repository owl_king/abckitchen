<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class UserMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if (\Auth::check()){
            if (\Auth::user()->role != 1){
                if (Request::is('home')){
                    $username = Auth::user()->name;
                    return view('home', compact('username'));
                }else{
                    return redirect('home');
                }
            }else{
                return $next($request);
            }
        }
        else{
                return redirect('auth/login');
        }
	}

}
