@extends('...backend')

@section('title')
    Người lao động
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <button class="btn btn-success btn-xs btn-all" id="huydanhdau">Hủy đánh dấu&nbsp;<span class="glyphicon glyphicon-ban-circle"></span></button>
                    <button class="btn btn-danger btn-xs btn-all" id="xoanhieu">Xóa nhiều&nbsp;<span class="glyphicon glyphicon-trash"></span></button>
                </div>
                <div class="col-md-4 title-search">
                    <form action="{{ url('/admin/users/showMembers/search') }}" method="get" class="search-form">
                        <div class="form-group has-feedback">
                            <label for="search" class="sr-only">Search</label>
                            <input type="text" class="form-control search-mem" name="search" id="search" placeholder="Tìm kiếm">
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </form>
                </div>
                <div>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-users"></i> DANH SÁCH NGƯỜI LAO ĐỘNG
                        </li>
                    </ol>
                </div>
            </div>
            <hr style="height: 0px">
            <div class="table-responsive">


                <table id="mytable" class="table table-bordred table-striped">

                    <thead>

                    <th><input type="checkbox" id="checkall" /></th>
                    <th><a class="title-row" href="{{ "showMembers?page=1&title=name&sort=".((isset($data) && !empty($data))?(($data['sort']=='asc')?("desc"):("asc")):"asc") }}">
                            Tên tài khoản
                        </a><span class="{{ (isset($data) && !empty($data))?(($data['title']=='name')?(($data['sort']=='asc')?'glyphicon glyphicon-chevron-down':'glyphicon glyphicon-chevron-up'):""):"" }}"></span>
                    </th>
                    <th>Email</th>
                    <th><a class="title-row" href="{{ "showMembers?page=1&title=created_at&sort=".((isset($data) && !empty($data))?(($data['sort']=='asc')?("desc"):("asc")):"asc") }}">
                            Ngày tạo
                        </a><span class="{{ (isset($data) && !empty($data))?(($data['title']=='created_at')?(($data['sort']=='asc')?'glyphicon glyphicon-chevron-down':'glyphicon glyphicon-chevron-up'):""):"" }}"></span>
                    </th>
                    <th><a class="title-row" href="{{ "showMembers?page=1&title=updated_at&sort=".((isset($data) && !empty($data))?(($data['sort']=='asc')?("desc"):("asc")):"asc") }}">
                            Ngày sửa
                        </a><span class="{{ (isset($data) && !empty($data))?(($data['title']=='updated_at')?(($data['sort']=='asc')?'glyphicon glyphicon-chevron-down':'glyphicon glyphicon-chevron-up'):""):"" }}"></span>
                    </th>
                    <th>Thanh toán</th>
                    <th>Sửa</th>
                    <th>Xóa</th>
                    </thead>
                    <tbody>

                    @if(count($allMem) && !empty($allMem))
                        @foreach($allMem as $mem)
                            <tr>
                                <td><input type="checkbox" class="checkthis" /></td>
                                <td>{{ $mem->name }}</td>
                                <td>{{ $mem->email }}</td>
                                <td>{{ $mem->created_at->format('d-m-Y') }}</td>
                                <td>{{ $mem->updated_at->format('d-m-Y') }}</td>
                                <td><a href="#" class="detail-admin" id="{{$mem->id}}" data-toggle="modal" data-target="#pay-modal">Chi tiết <i class="fa fa-exclamation-circle"></i></a></td>
                                <td><p data-placement="top"  title="Edit"><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#edit-member" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                                <td><p data-placement="top"  title="Delete"><button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-member" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" style="text-align: center">Không có dữ liệu</td>
                        </tr>
                    @endif

                    <!-- Modal -->
                    <div class="modal fade" id="pay-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Thông tin thanh toán</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="col-sm-3 css-head"><h3>Tháng</h3></div>
                                    <div class="col-sm-3 css-head"><h3>Tổng tiền</h3></div>
                                    <div class="col-sm-3 css-head"><h3>Tình trạng</h3></div>
                                    <div class="col-sm-3 css-head"><h3>Thao tác</h3></div>
                                    <select class="col-sm-3 select-month-admin">
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                    <div class="col-sm-3 total-cost-admin">0</div>
                                    <div class="col-sm-3 check-state-admin">Chưa thanh toán</div>
                                    <div class="col-sm-3"><button style="margin-right: 5px; margin-top: 0px;margin-bottom: 20px;" class="btn btn-success btn-xs set-thanhtoan"><span class="glyphicon glyphicon-ok-sign"></span></button><button style="margin-top: 0px;margin-bottom: 20px;" class="btn btn-danger btn-xs huy-thanhtoan"><span class="glyphicon glyphicon-remove-sign"></span></button></div>
                                </div>
                                <div style="clear:both;"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn btn-primary submit-state-admin">Lưu lại</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    </tbody>

                </table>

                <div class="clearfix"></div>
                {!! (isset($data) && !empty($data))?($allMem->appends(['title' => $data['title'], 'sort' => $data['sort']])->render()):($allMem->render()) !!}

            </div>

        </div>
    </div>
    </div>


    <div class="modal fade" id="edit-member" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Chỉnh sửa thông tin</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user-name" class="control-label">Tên tài khoản:</label>
                        <input class="form-control " type="text">
                    </div>
                    <div class="form-group">
                        <label for="user-name" class="control-label">Email:</label>
                        <input class="form-control " type="text">
                    </div>
                    <div class="form-group">
                        <label for="user-name" class="control-label">Liên hệ:</label>
                        <input class="form-control " type="text">
                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-primary btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Cập nhật</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="delete-member" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Xóa tài khoản</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Bạn có chắc muốn xóa tài khoản này?</div>

                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Có</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Không</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@stop