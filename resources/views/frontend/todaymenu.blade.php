<section id="tomorrow-menu">
    <div class="container-div">
        <div class="row">
            <div>
                <label class="home-tmr-lable">
                    <h2 class="heading">Thực đơn ngày mai</h2>
                    <a href="{{ url('/ordermenu#tomorrow-menu-order') }}" class="btn btn-default btn-see-more-tmr"><i class="fa fa-info-circle"></i> Xem chi tiết</a>
                </label>
                <div style="clear:both"></div>
            </div>

            @if( isset($food_menu) )
                <div class="list-food">
                    @foreach ($food_menu as $foodmenu)
                        <figure>
                            <img src="{{ $foodmenu->food->img }}">
                            <figcaption>
                                <p>{{ $foodmenu->food->name }}</p>
                            </figcaption>
                        </figure>
                    @endforeach
                </div>
            @else
                <h3>Hiện tại chưa có món ăn nào được thiết lập cho ngày mai</h3>
            @endif
        </div>
    </div>
</section>