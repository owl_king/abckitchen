<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Usermenu extends Model {

    protected $table = 'user_menu';

    protected $fillable =['user_id', 'menu_id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

}
