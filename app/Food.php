<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model {

    protected $table = 'foods';

    protected $fillable =['name', 'cost', 'img'];

    public $timestamps = false;

    public function foodmenu()
    {
        return $this->hasMany('App\Foodmenu');
    }

}
