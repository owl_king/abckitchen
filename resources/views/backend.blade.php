<!DOCTYPE html">
<html>
<head>
    <title>@yield('title')</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="_token" content="{{ csrf_token() }}"/>

    <link type="image/x-icon" rel="shortcut icon" href="http://static.bizwebmedia.net/favicon.ico">

    <link href="{{ asset('/css/metro-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/iconFont.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-theme.min.css') }}">
    <link href="{{ asset('/css/sb-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/latest.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">

    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.widget.min.js') }}"></script>
    <script src="{{ asset('/js/metro.min.js') }}"></script>
    <script src="{{ asset('/js/metro-global.js') }}"></script>
    <script src="{{ asset('/js/metro-locale.js') }}"></script>
    <script src="{{ asset('/js/metro-calendar.js') }}"></script>
    <script src="{{ asset('/js/new.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-typeahead.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/sweetalert.min.js') }}"></script>
	<script src="{{ asset('/js/admin.js') }}"></script>
    <script src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script src="{{ asset('/js/toucheffects.js') }}"></script>
	<style type="text/css">#Header{background-image: url( {{ asset('images/logo-admin.png') }} ); height:103px; background-repeat: no-repeat;}</style>
</head>
<body>

		<div class="bg_topmenu">
			<nav class="TopMenu">
				<ul class="clearfix">
					<li style="color: #FFFFFF">Xin chào:</li>
					<li class="dropdown">
						<a class="username" href="#">{{ $username }}<span class="arrow"></span></a>
						<ul class="sub-menu">
							<li><a href="#">Thông tin cá nhân</a></li>
							<li><a href="#">Thông tin cá nhân</a></li>
							<li><a href="#">Thông tin cá nhân</a></li>
							<li><a href="#">Thông tin cá nhân</a></li>
						</ul>
					</li>
					<li class="topMenu_Cost">
						<a href="#">
							Thông báo
						</a>
					</li>
					<li class="topMenu_Favorite">
						<a href="{{ url('/home') }}">
							Quay lại trang chủ
						</a>
					</li>

					<li class="topMenu_Logout">
						<a href="{{ url('/auth/logout') }}">
							Thoát
						</a>
					</li>
				</ul>
			</nav>
		</div>
        <div class="Clear"></div>
		<div id="bg_header">
			<div id="Header">
				<div id="Logo">
				    <div id="LogoContainer">
				        <h1><a href="{{ url('/admin') }}"><span>ABCKITCHEN</span></a></h1>
                    </div>
                </div>
				<div id="Menu">
					<div class="menu-container">
						<ul id="nav">
							<li id="nav-1" class="level0 nav-1 first parent actived-menu">
								<a href="{{asset('/admin')}}">
									<span>Trang chủ</span>
								</a>
							</li>

							<li id="nav-2" class="level0 nav-2 parent">
								<a href="{{ url('admin/menus') }}">
									<span>Quản lý thực đơn</span>
								</a>
							</li>

							<li id="nav-3" class="level0 nav-3 parent">
                                <a href="{{asset('/admin/foods')}}">
                                    <span>Món ăn</span>
                                </a>

                            </li>

							<li id="nav-4" class="level0 nav-4 parent">
								<a href="#">
									<span>Thành viên</span>
								</a>
								<ul class="level0">
									<li id="nav-3-1" class="level1 nav-3-1">
										<a href="{{asset('/admin/users/showManagers')}}">
											<span>Quản trị viên</span>
										</a>
									</li>

									<li id="nav-3-2" class="level1 nav-3-2" >
										<a href="{{asset('/admin/users/showMembers')}}">
											<span>Người lao động</span>
										</a>

									</li>

								</ul>
							</li>

							<li id="nav-5" class="level0 nav-5 parent">
								<a href="#">
									<span>Cấu hình</span>
								</a>

							</li>

						</ul>
					</div>

                    <br class="Clear">
                </div>
				<div class="Clear"></div>
			</div>
		</div>
		<div id="Container">

                <div id="Wrapper">
					<div class="Clear"></div>
					<div id="cphMain_Content">

									@yield('content')

					</div>
                </div>
                <div class="Clear"></div>

        </div>

        @include('footer')